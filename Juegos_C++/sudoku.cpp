#include<iostream>
#include<stdlib.h>

using namespace std;

int matriz[9][9];

bool esGanador();
char letra(int n);
void mostrar();
void inicializar();
void llenanoInicial();
bool evaluar(int *vector);
void jugar(int f, int c, int n);

int main(){
    int fil,col,num;

    inicializar(); // Inicializa la matriz para que toda sea igual a 0
    llenanoInicial();  // Llea la matriz con valores iniciales

do{
   system("cls");
   cout<<"\n\n\n";
   mostrar();
     cout<<endl;

     do{
       cout<<"\tSeleccione Fila: ";
       cin>>fil;
     }while(fil<1 || fil>9);

     do{
        cout<<"\tSeleccione Columna: ";
        cin>>col;
     }while(col<1 || col>9);
     cout<<endl;
     do{
        cout<<"\t(Para eliminar de la casilla ingrese 0)\n\tIngrese Numero a Jugar: ";
        cin>>num;
     }while(num<0 || num>9);

jugar(fil-1, col-1, num);

}while(!esGanador());

cout<<". . : : F E L I C I T A C I O N E S : : . ."<<endl;

system("pause");
return 0;
}

bool esGanador(){
    int i,j;
    for(i=0; i<9; i++){
        for(j=0; j<9; j++){
            if(matriz[i][j] == 0){
                 return false;
            }
        }
    }
    return true;
}

char letra(int n){
     if(n == 0){
          return ' ';
     }else{
          return (char)n+48;
     }
}

void mostrar(){
int i,j;
     cout<<"\t+-----+-----+-----+ Filas"<<endl;
     for(i=0; i<9; i++){
         cout<<"\t|";
         for(j=0; j<9; j++){
             cout<<letra(matriz[i][j])<<"|";
         }
         cout<<i+1<<endl;

         if(i==2 || i==5 || i==8){
             cout<<"\t+-----+-----+-----+"<<endl;
         }else{
             cout<<"\t|-+-+-|-+-+-|-+-+-|"<<endl;
         }
     }
     cout<<"\t";
     for(j=0; j<9; j++){
         cout<<" "<<j+1;
     }
     cout<<endl;
     cout<<"\t Columnas"<<endl;
}

void inicializar(){
    int i,j;
    for(i=0; i<9; i++){
        for(j=0; j<9; j++){
            matriz[i][j] = 0;
        }
    }
}

void llenanoInicial(){
int i;
srand(time(NULL));
     for(i=1; i<=9; i++){
         matriz[rand()%9][rand()%9] = i;
     }
}

bool evaluar(int *vector){
     int i,j;
         for(i=0; i<9; i++){
             for(j=0; j<9; j++){
                 if(i!=j && vector[i]!=0 && vector[i] == vector[j]){
                        return false;
                 }
             }
         }
         cout<<endl;
     return true;
}

void jugar(int f, int c, int n){
     int i,j,ri,rj,x=0, ant;
     int aux[9];
     bool volver = false;

     ant = matriz[f][c];
     matriz[f][c] = n;

     // Evaluar Fila
     for(i=0; i<9; i++){
         aux[i] = matriz[f][i];
     }
     if(!evaluar(aux)){
        cout<<"Numero Repetido en Fila"<<endl;
        volver = true;
     }

     // Evaluar Columna
     for(i=0; i<9; i++){
         aux[i] = matriz[i][c];
     }
     if(!evaluar(aux)){
        cout<<"Numero Repetido en Columna"<<endl;
        volver = true;
     }

     // Evaluar Region
     ri = (f/3) * 3;
     rj = (c/3) * 3;

     for(i=ri; i<(ri+3); i++){
         for(j=rj; j<(rj+3); j++){
             aux[x] = matriz[i][j];
             x++;
         }
     }
     if(!evaluar(aux)){
        cout<<"Numero Repetido en Region"<<endl;
        volver = true;
     }

     if(volver){
        matriz[f][c] = ant;
     }
     
system("pause");
}
