/********************************************************************************/
/*Autor: Axel Díaz								*/
/*Programa: Adivinar Palabra (juego)						*/
/*Fecha: 5/10/2010								*/
/*Version: 1.0									*/
/*Correo: axelio8@gmail.com							*/
/********************************************************************************/

#include <string.h>
#include <iostream>
#include <time.h>
#include <stdlib.h>
using namespace std;
int tam;

int Pistas();
char GenPal(int op);
//*******DECLARACION DE VECTORES******//
char Palabra[50];
//***********************************//
int inicializar(char *V);
int lleno(char *V);

int lleno(char *V, int pos){
	for(int i=0; i<pos; i++){
		if(V[pos]==0){
			return 0;
		}else return 1;
	}
}

int inicializar(char *V){
	for(int i=0; i<50; i++){
		V[i]=0;
	}
}

char GenPal(int op){
	char Vector[50];
	int pos, cont=0;
//*****INICIALIZACION DE VECTORES******//
	inicializar(Vector);

	cout<<"La palabra es:";
	if (op==1) {
		tam=6;
		strcpy(Vector, "europa");
		cont=0;
		while(cont<7){
			pos=1+rand()%6;
			if(pos==0&&(Palabra[pos])==0){
				Palabra[pos]='e';
				cont++;
			}
			if(pos==1&&(Palabra[pos])==0){
				Palabra[pos]='u';
				cont++;
			}
			if(pos==2&&(Palabra[pos])==0){
				Palabra[pos]='r';
				cont++;
			}
			if(pos==3&&(Palabra[pos])==0){
				Palabra[pos]='o';
				cont++;
			}
			if(pos==4&&(Palabra[pos])==0){
				Palabra[pos]='p';
				cont++;
			}
			if(pos==5&&(Palabra[pos])==0){
				Palabra[pos]='a';
				cont++;
			}
		}
		
	}
	else if (op==2) strcpy(Vector, "axel");
	else if (op==3) strcpy(Vector, "mango");
	else if (op==4) strcpy(Vector, "canguro");
	else strcpy(Vector, "tenis");
	
	for(int i=0; i<50; i++){
		cout<<Vector[i];
	}
	cout<<". Con la opcion numero "<<op;
	
	cout<<"\nEl vector palabra es: ";
	for(int i=0; i<tam-1; i++){
		cout<<Palabra[i];
	}
}
	
int Pistas(){
	int caso, op;
	op=1+rand()%5;
	switch(op){
		case 1:
			cout<<"Lugares\n\n";
			caso=1;
		break;
		
		case 2:
			caso=2;
			cout<<"Nombres\n\n";
		break;
		
		case 3:
			caso=3;
			cout<<"Frutas\n\n";
		break;
		
		case 4:
			caso=4;
			cout<<"Animales\n\n";
		break;
		
		case 5:
			caso=5;
			cout<<"Deportes\n\n";
		break;
		
	}
	return caso;
}

//************DECLARACIONES************//
int main(){
char resp;
int num, opcion;
inicializar(Palabra);

//************************************//

system("reset");
srand(time(NULL));
do{
	
//********************PISTAS******************//
	cout<<"Acerca de: ";
	opcion=Pistas();
//*************GENERAR PALABRAS***************//
	GenPal(opcion);
	
	
	cout<<"\n\nQue palabra es? -> ";
	

cout<<"\nDesea repetir el juego? (Sí/No) ";
cin>>resp;
system("reset");
}while(resp=='s'||resp=='S');
system("reset");
cout<<"Adios! >=/";
cout<<"\n\n\n";
return 0;
}