#include<iostream>
#define CANT 20
#define NUM 100
using namespace std;

void mostrar(int *v){
int i;
	for(i=0; i<CANT; i++){
		cout<<v[i]<<" ";
	}
}

void llenar(int *v){
int i;
srand(time(NULL));
	for(i=0; i<CANT; i++){
		v[i] = 1+rand()%NUM;
	}
}

int main(){

int vector[CANT];
int i,j,aux,aux1,n;

llenar(vector);
system("cls");
	cout<<"Direccion de Memoria: "<<&vector<<endl;

cout<<"Vector Desorganizado:\n\t";
mostrar(vector);
cout<<endl;

//------ Metodo de la Burbuja ------//
for(i=0; i<CANT;i++){
	for(j=0; j<CANT-1; j++){
		if(vector[j] > vector[j+1]){
			aux = vector[j];
			vector[j] = vector[j+1];
			vector[j+1] = aux;
		}
	}
}

cout<<"\nMetodo de la Burbuja\nAscendente:\n\t";
mostrar(vector);
cout<<endl;


//------ Metodo de Seleccion ------//
for(i=0; i<CANT;i++){
	aux = vector[i];
	for(j=i; j<CANT; j++){
		if(vector[j] > aux){
			aux = vector[j];
			n = j;
		}
	}
	vector[n] = vector[i];
	vector[i] = aux;
}

cout<<"\nMetodo Seleccion\nDescendente:\n\t";
mostrar(vector);
cout<<endl;

cin.get();cin.get();
return 0;
}
