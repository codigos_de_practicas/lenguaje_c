/********************************************************/
/*Nombre: Ejercicio 6 de la Guía 1                      */
/*Autor: Axel Díaz                                      */
/*Correo: diaz.axelio@gmail.com                         */
/*Fecha: 22/10/2010                                     */
/********************************************************/

/***********************ENUNCIADO************************/
/*Realice un programa en C++ que calcule la serie de    */ 
/*fibonacci, mientras la serie sea menor que N.         */
/********************************************************/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
main(){
char Resp;
do{
system("reset");
int num, fibo=0, a=1, b=1, c;
	cout<<"Introduzca el número para calcula la serie fibonacci: ";
	cin>>num;
	
	do{
	if(a<=num){
		c = a + b;
		}
		if(c<=num){
			b = c + a; 
			}
			if (b<=num){
				a = b +c;	
			}
	cout<<c<<endl<<b<<endl<<a<<endl;
	}while(c<=num || a<=num || b<=num);
cout<<"\nDesea repetir el proceso? (S/N) ";
cin>>Resp;
}while(Resp=='S'||Resp=='s');
system("reset");
cout<<"Hasta luego =)\n";
}
