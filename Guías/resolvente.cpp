#include <iostream>
#include <math.h>

using namespace std;

int main () {
    int a,b,c;
    float x1,x2,y;
     cout<<"introduzca el valor de A ";
     cin>>a;     
     cout<<"introduzca el valor de B ";
     cin>>b;
     cout<<"introduzca el valor de C ";
     cin>>c;

	if(a == 0){
		cout << "\aError: Division por 0"<<endl;
	}else{
		y = (pow(b,2))-(4*a*c);
		if(y < 0){
			cout <<"\aError: Raiz de Numero Negativo"<<endl;
		}else{
			x1=(-b+sqrt(y)/(2*a));
			x2=(-b-sqrt(y)/(2*a));

			cout << "X1 es "<<x1<<endl;
			cout << "X2 es "<<x2<<endl;
		}
	}

return 0;
}
