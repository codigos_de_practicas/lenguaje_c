//A un trabajador le pagan seg�n sus horas trabajadas y la tarifa est� a un valor por hora. Si la
//cantidad de horas trabajadas es mayor a 40 horas, la tarifa por hora se incrementa en un 50% para las
//horas extras. Calcular el salario del trabajador dadas las horas trabajadas y la tarifa.

#include <iostream>
using namespace std;
int main(){
    int ht, htotal, he;
    float salariototal, ph, salario, phe, salariohe;
    char resp='s';
 

    cout<<"Por favor, introduzca las horas que ha trabajado ";
    cin>>ht;
    cout<<"Introduzca el valor de cada hora trabajada ";
    cin>>ph;
    
    salario=ph*ht;
    salariohe=0;
 
    if (ht>40){
               he=ht-40;
               phe=ph+ph*50/100;
               
               salariohe=phe*he;
               
               salario=salario+salariohe;
               }
               
    cout<<"Por haber trabajado "<<ht<<" horas tienes "<<he<<" horas extras. De los cuales se te agregaran "<<salariohe<<" bolivares"<<endl;
    cout<<"Por lo tanto tu pago total sera de "<<salario<<" bolivares";
    cout<<endl;

    cin.get();cin.get();
    return 0;
}
