/********************************************************/
/*Nombre: Ejercicio 4 de la Guía 1                      */
/*Autor: Axel Díaz                                      */
/*Correo: diaz.axelio@gmail.com                         */
/*Fecha: 20/10/2010                                     */
/********************************************************/

/***********************ENUNCIADO************************/
/*Realice un programa en C++ que calcule                */
/*el factorial de un número.                            */
/********************************************************/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

float factorial(double n);
float factorial(double n){
	float fact=1;

	for(int i=n; i>=1; i--){
	fact=fact*i;
	}
	return fact;
}

main(){
	char Resp;
	double num;
do{
	system("reset");
	cout<<"Introduzca el número a calcular el factorial: ";
	cin>>num;
	while(num<0) {
		cout<<"ERROR! El número no puede ser negativo\nPor favor intente de nuevo: ";
		cin>>num;
	}
	
	cout<<"\n\nEl factorial de "<<num<<" es: "<<factorial(num);
	
cout<<"\nDesea repetir el proceso? (S/N) ";
cin>>Resp;
}while(Resp=='S'||Resp=='s');
system("reset");
cout<<"\n\nHasta luego =)\n";

}
