/********************************************************/
/*Nombre: Ejercicio 5 de la Guía 1                      */
/*Autor: Axel Díaz                                      */
/*Correo: diaz.axelio@gmail.com                         */
/*Fecha: 20/10/2010                                     */
/********************************************************/

/***********************ENUNCIADO************************/
/*Realice un programa en C++ que calcule la sumatoria   */ 
/*de los números pares e impares desde el 0 hasta N.    */
/********************************************************/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
int pares(int num);
int impares(int num);

int pares(int num){
	int par=0;
	for (int i=0; i<=num; i++){
		if(i%2==0){
			par=par+i;
		}
	}
	return par;
}


int impares(int num){
	int impar=0;
	for (int i=0; i<=num; i++){
		if(i%2==1){
			impar=impar+i;
		}
	}
	return impar;
}

main(){
char Resp;
int n;
do{
system("reset");

	cout<<"Introduzca el número para hacer la sumatoria por favor: ";
	cin>>n;
	
	cout<<"\n\nLa sumatoria de los números pares desde el 0 hasta "<<n<<" es: "<<pares(n);
	cout<<"\nY la sumatoria de los números pares desde el 0 hasta "<<n<<" es: "<<impares(n);
	
cout<<"\n\nDesea repetir el proceso de nuevo? (S/N) ";
cin>>Resp;
}while(Resp=='s'||Resp=='S');
system("reset");
cout<<"Hasta luego =). Por cierto, desea apagar la computadora? (S/N) \n";
cin>>Resp;
if(Resp=='n'||Resp=='N'){cout<<"Vale... ;) \n";}
else{ system("sudo halt");}
}
