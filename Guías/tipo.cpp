#include <iostream>
using namespace std;

int main(){

cout<<"El tamaño de int es "<<sizeof(int)<<" bytes"<<endl;
cout<<"El tamaño de long long int es "<<sizeof(long long int)<<" bytes"<<endl;
cout<<"El tamaño de float es "<<sizeof(float)<<" bytes"<<endl;
cout<<"El tamaño de double es "<<sizeof(double)<<" bytes"<<endl;
cout<<"El tamaño de char es "<<sizeof(char)<<" bytes"<<endl;
	
return 0;
}
