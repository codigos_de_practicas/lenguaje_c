#include<iostream>
#include "cpila.h"
using namespace std;
int main(){
PilaGenerica<int> p1, p2;
int c1 = 0, c2 = 0, acum1 = 0, acum2 = 0, dato;
float prom = 0;
char r = 'S';
cout << "CARGA DE DATOS EN LAS PILAS" << endl;
do{
cout << "Ingrese numero: ";
cin >> dato;
if(dato%2==0){
p1.meter(dato);
c1++;
acum1+=dato;
}
else{
p2.meter(dato);
c2++;
acum2 = acum2 + dato;
}
cout << "Desea ingresar otro dato(s/n): ";
cin >> r;
}while(r=='s' || r=='S');
cout << "RESULTADOS:" << endl;
cout <<"Pila Uno:" << endl;
while(!p1.vacia()){
cout << p1.sacar() << endl;
}
cout << "Total de datos cargados en la pila uno es: " << c1 << endl;
cout << "Pila Dos: " << endl;
while(!p2.vacia()){
cout << p2.sacar() << endl;
}
cout << "Total de datos cargados en la pila dos es: " << c2 << endl;
if(c1>c2){
cout <<"Pila uno contiene mas datos que la Pila dos" << endl;
}
else{
if(c2>c1){
cout << "Pila dos contiene mas datos que la Pila Uno: " << endl;
}
else{
cout << "Pila Uno y Pila dos contiene la misma cantidad de datos" << endl;
}
}
prom = (float)(acum1+acum2)/(c1+c2);
cout << "El promedio de los datos cargados en la pilas es: " << prom << endl;
return 0
