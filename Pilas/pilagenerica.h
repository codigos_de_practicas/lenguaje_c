/**************************Pila Genérica*********************************/
template <class T>
class PilaGenerica{
class NodoPila{
public:
NodoPila *siguiente;
T elemento;
NodoPila(T x){
elemento = x;
siguiente = NULL;
}
};
NodoPila *cima;
public:
//Constructor de la clase pilagenerica
PilaGenerica(){
cima = NULL;
}
//Método para añadir elementos a la pila
void meter(T elemento);
//Método para sacar elementos de la pila
T sacar(); const
//Método para consultar el elemento en la cima de pila
T cimaPila(); const
bool pilaVacia();
void limpiarPila();
~PilaGenerica(){
limpiarPila();
}
};
//Implementación Métodos
//Método para la Verificación del estado de la pila
template <class T>
const bool PilaGenerica<T>::pilaVacia()
{
return cima == NULL;
}
//Método para poner un elemento en la pila
template <class T>
void PilaGenerica<T>::meter(T elemento){
NodoPila *nuevo;
nuevo = new NodoPila(elemento);
nuevo->siguiente = cima;
cima = nuevo;
}
//Método para sacar un elemento de la pila
template <class T>
T PilaGenerica<T>::sacar(){
if(pilaVacia()){
throw "Pila vacía, no se puede extraer elementos!";
}
T aux = cima->elemento;
cima = cima->siguiente;
return aux;
}
//Método para la obtención del elemento cima de la pila
template <class T>
const T PilaGenerica<T>::cimaPila(){
if(pilaVacia())
throw "Pila Vacía";
return cima->elemento;
}
//Método para el vaciado de la pila
template <class T>
void PilaGenerica<T>::limpiarPila(){
NodoPila *n;
while(!pilaVacia()){
n = cima;
cima = cima->siguiente;
delete n;
}
}

