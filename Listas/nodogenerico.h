template <class T>
class NodoGenerico{

protected:
	T info;
	NodoGenerico * enlace;

public:
	NodoGenerico(){
	enlace = 0;
	}

	NodoGenerico(T dato){
	info = dato;
	}

T infoNodo();

void ponerInfo(T dato);
NodoGenerico<T> * enlaceNodo() const;

void ponerEnlace(NodoGenerico<T> * n);
};


template <class T>
T NodoGenerico<T>::infoNodo(){
return this-> info;
}

template <class T>
void NodoGenerico<T>::ponerInfo(T dato){
this-> info = dato;
}

template <class T>
NodoGenerico<T> * NodoGenerico<T>::enlaceNodo() const{
return this-> enlace;
} 

template <class T>
void NodoGenerico<T>::ponerEnlace(NodoGenerico<T> * n){
this-> enlace = n;
}





