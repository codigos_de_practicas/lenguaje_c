/************************************************
*Titulo: empleados.h				*
*Objetivo: Definicion de la clase empleado	*
*Autor: Axelio / axelio8@gmail.com		*
*Fecha: 17/10/09				*
*************************************************/
//{}

/*
1.Crear una clase Empleado, usando los siguientes atributos: cedula, apellidos, nombres, tipo de empleado, Nro de hijos,Horas trabajadas y pago por horas.

a.El tipo de empleado son tres: Obrero, Administrativo y Profesor.-----
b.Los Obreros ganan Bs. 100 la hora, el administrativo Bs 150 y el Docente Bs. 250.-----
c.Calcular  usando una función get el Sueldo Básico, que equivale a las horas trabajadas * el pago por hora.-----
d.Todo empleado gana un bono por hijo de Bs. 100, la universidad reconoce hasta un máximo de 3 hijos. Calcular el bono por hijo usando un get.----
e.Todo empleado le descuenta por seguro social un 5% del sueldo básico, calcular el aporte del seguro social usando un get.------
f.Todo empleado aporta el 1% del sueldo básico por concepto de Ley  Política Habitacional, calcular la LPH usando un get.------
g.Calcular el Sueldo neto, que equivale al sueldo básico más bono por hijo menos Seguro social y LPH.-----
h.Debe usar sobrecarga de operadores de entrada y salida.*/

#ifndef _EMPLEADO_H
#define _EMPLEADO_H
#include <iostream>
#include <string>
using namespace std;

class cEmpleado{

	double cedula;
	string apellidos; 
	string nombres;
	int tipo_de_empleado;
	double nro_de_hijos;
	double horas_trabajadas; 
	double pago_por_horas;

public:
	cEmpleado();
	cEmpleado(double,string,string,int,double,double,double);

	//---------- calculos ----------------

	void Cedula(double xcedula){this->cedula=xcedula;}
	double Cedula(){return this->cedula;}

	void Nombre(string xnombres){this->nombres=xnombres;}
	string Nombre(){return this->nombres;}

	void Apellidos(string xapellidos){this->apellidos=xapellidos;}
	string Apellidos(){return this->apellidos;}	

	void Tipo_de_empleado(int xtipo_de_empleado){this->tipo_de_empleado=xtipo_de_empleado;}
	int Tipo_de_empleado(){return this->tipo_de_empleado;}

	void Nro_de_hijos(double xnro_de_hijos){this->nro_de_hijos=xnro_de_hijos;}
	double Nro_de_hijos(){return this->nro_de_hijos;}	

	void Horas_trabajadas(double xhoras_trabajadas){this->horas_trabajadas=xhoras_trabajadas;}
	double Horas_trabajadas(){return this->horas_trabajadas;}

	void Pago_por_horas(double xpago_por_horas){this->pago_por_horas=xpago_por_horas;}
	double Pago_por_horas(){return this->pago_por_horas;}


//-----------------------------	Sueldo Básico
	double Sueldo_Base(){
	double sb;
	if(this->tipo_de_empleado == 1) { sb = this->horas_trabajadas * 100;
		}else{ if(this->tipo_de_empleado == 2) { sb = this->horas_trabajadas * 150;
			}else{ sb = this->horas_trabajadas * 250;
		}
	}
	return sb;}

//--------------------------------Bono
	int Bono(){
	int Bono;

	if(this->nro_de_hijos==0){ Bono=0;}
	  else if (this->nro_de_hijos==1){ Bono=100;}
		else if (this->nro_de_hijos==2){Bono=200;}
		      else if((this->nro_de_hijos>=3)){Bono=300;}

	return Bono;
	}
	
//----------------------------Seguro social
	double Seguro(){
	double seguro;
	seguro=Sueldo_Base()*0.05;
	return seguro;
	}

//----------------------Ley de Politica Habitacional

	double LPH(){
	double lph;
	lph=Sueldo_Base()*0.01;
	return lph;
	}

//----------------------Sueldo Neto
	double Sueldo_Neto(){
	double sueldo_neto;

	sueldo_neto=(Sueldo_Base()+Bono())-(Seguro()+LPH());
	return sueldo_neto;
	}

int Salida();
int Entrada();

/*//-------------------------Sobrecarga de operadores
	friend istream & operator >>(istream &,cEmpleado &);
	friend ostream & operator <<(ostream &,cEmpleado &);
	*/

	};

	#endif
