/************************************************
*Titulo: empleados.cc				*
*Objetivo: Implementacion de la clase Empleado	*
*Autor: Axelio / axelio8@gmail.com		*
*Fecha: 17/10/09				*
*************************************************/
//{}

/* Pasos
7) Empiezo a hacer el constructor luego del encabecado y eso, que lo copio del principal...
8) viendo el 'atletas' veo que no hay using namespaces ni un main ni nada..

*/

#include "empleados.h"



cEmpleado::cEmpleado(double xcedula,string xapellidos,string xnombres,int xtipo_de_empleado,double xnro_de_hijos,double xhoras_trabajadas,double xpago_por_horas){

	this->cedula=xcedula;
	this->apellidos=xapellidos; 
	this->nombres=xnombres;
	this->tipo_de_empleado=xtipo_de_empleado;
	this->nro_de_hijos=xnro_de_hijos;
	this->horas_trabajadas=xhoras_trabajadas; 
	this->pago_por_horas=xpago_por_horas;
}

cEmpleado::cEmpleado(){

	this->cedula=0;
	this->apellidos=""; 
	this->nombres="";
	this->tipo_de_empleado=0;
	this->nro_de_hijos=0;
	this->horas_trabajadas=0; 
	this->pago_por_horas=0;
}


//-------Sobrecarga de Entrada------

int cEmpleado::Entrada(){
	string xapellidos, xnombres;
	int xtipo_de_empleado;
	double xcedula,xnro_de_hijos, xhoras_trabajadas, xpago_por_horas;

	cout<<"\nCedula "; cin>>xcedula;
	cout<<"\nApellido "; cin>>xapellidos;
	cout<<"\nNombre "; cin>>xnombres;
	cout<<"\nTipo de Empleado: \n1. Obrero \n2. Administrativo \n3. Profesor "; cin>>xtipo_de_empleado;
	cout<<"\nNumero de hijos "; cin>>xnro_de_hijos;
	cout<<"\nHoras trabajadas "; cin>>xhoras_trabajadas;

	this->Cedula(xcedula);
	this->Apellidos(xapellidos); 
	this->Nombre(xnombres);
	this->Tipo_de_empleado(xtipo_de_empleado);
	this->Nro_de_hijos(xnro_de_hijos);
	this->Horas_trabajadas(xhoras_trabajadas); 
	return 1;
		             
}

//-------------------------Salida------------

int cEmpleado::Salida(){
	string xapellidos, xnombres;
	int xtipo_de_empleado;
	double xcedula,xnro_de_hijos, xhoras_trabajadas, xpago_por_horas;

	cout<<"\nCedula..................:"<<this->Cedula();
	cout<<"\nApellidos...............:"<<this->Apellidos();
	cout<<"\nNombre..................:"<<this->Nombre();
	cout<<"\nTipo de Empleado........:";
	if(xtipo_de_empleado == 1) { cout<<"Obrero";
		}else{ if(xtipo_de_empleado == 2) { cout<<"Administrativo";
			}else{ cout<<"Profesor";
        		  }
             }
	cout<<"\nNumero de hijos................:"<<this->Nro_de_hijos();
	cout<<"\nHoras trabajadas...............:"<<this->Horas_trabajadas();
	cout<<"\nSueldo Base....................:"<<this->Sueldo_Base();
	cout<<"\nBono.......................... :"<<this->Bono();
	cout<<"\nCuota por Seguro Social........:"<<this->Seguro();
	cout<<"\nLey de Politica Habitacional...:"<<this->LPH();
	cout<<"\nSueldo Neto....................:"<<this->Sueldo_Neto()<<endl;
	return 1;
}





