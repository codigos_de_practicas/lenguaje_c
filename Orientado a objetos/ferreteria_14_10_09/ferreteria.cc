/****************************************************************
* Programa: ferreteria.cc					*
* Objetivo: implementación de la clase Ferreteria				*
* Fecha: 30/09/2009						*
* Autor: jairo molina /www.jairomolina.net.ve/jairo_unerg@hotmail.com
******************************************************************/
#include "ferreteria.h"

//---------------> constructors sin paso de parametros---------------
Ferreteria::Ferreteria(){
	this->codigo=0;
	this->descripcion="";
	this->cantidad=0;
	this->precio_c=0;
}

//---------------> constructors con paso de parametros---------------
Ferreteria::Ferreteria(int xcodigo,string xdescripcion,int xcantidad,double xprecio_c){
	this->codigo=xcodigo;
	this->descripcion=xdescripcion;
	this->cantidad=xcantidad;
	this->precio_c=xprecio_c;
}



//--------------------> sobrecarga de entrada <------------------

istream & operator >>(istream &entrada,Ferreteria &objeto){

	int xcod,xcan;
	string xdes;
	double xprecio_c;

	cout<<"\n*** INGRESE ARTICULOS ***\n";
	cout<<"\nCodigo........:";entrada>>xcod;
	cout<<"\nDescripcion...:";entrada>>xdes;
	cout<<"\nCantidad......:";entrada>>xcan;
	cout<<"\nPrecio Compra.:";entrada>>xprecio_c;

	objeto.Codigo(xcod);
	objeto.Descripcion(xdes);
	objeto.Cantidad(xcan);
	objeto.Precio_c(xprecio_c);

	return entrada;
}

//--------------------> sobrecarga de salida <------------------

ostream & operator <<(ostream &salida,Ferreteria &objeto){

cout<<"\n*** ARTICULO ***\n";
	salida<<"\nCodigo........:"<<objeto.Codigo();
	salida<<"\nDescripcion...:"<<objeto.Descripcion();
	salida<<"\nCantidad......:"<<objeto.Cantidad();
	salida<<"\nPrecio Compra.:"<<objeto.Precio_c();
	salida<<"\nInversion.....:"<<objeto.Inversion();
	salida<<"\nPrecio Venta..:"<<objeto.Precio_v();
	salida<<"\nGanancia Total:"<<objeto.Ganancia()<<endl;
	return salida;
}




















