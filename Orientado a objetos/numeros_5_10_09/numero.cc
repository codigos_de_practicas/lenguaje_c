/************************************************************************
* Programa: numero.cc							*
* Objetivo: implementar la clase numero					*
* Fecha: 13/10/2008							*
* Autor: jairo MOlina / www.jairomolina.net.ve /jairo_unerg@hotmail.com	*
*************************************************************************/
#include "numero.h"
//------------------>> constructor   <<-----------------------------
Numero::Numero(double xfactorial,float xvalor){
	this->factorial=xfactorial;
	this->valor=xvalor;
}

//----------------->> sobrecarga de entrada <<------------------------

istream &operator >>(istream &entrada,Numero &objeto){
	int xnum;
	float xvalor;
	double acum=1;

	
	do{
		cout<<"\ningrese el valor a calcular ...:";entrada>>xnum;
		if(xnum<0){
			cout<<"\ndebe ser positivo\n";
			sleep(3);
		}
	}while(xnum<0);
	if(xnum==0) objeto.Factorial(1);
	else{
		for(int i=1;i<=xnum;i++) acum*=i;
		
		objeto.Factorial(acum);
	}

	cout<<"\nIngrese valor decimal...:";entrada>>xvalor;
	objeto.Valor(xvalor);
}

//----------------->> sobrecarga de salida <<------------------------

ostream &operator <<(ostream &salida,Numero &objeto){
	salida<<"\n**** Datos del Numero ***";
	salida<<"\nFactorial.......:"<<objeto.Factorial();
	salida<<"\nValor Decimal...:"<<objeto.Valor()<<endl;
}










