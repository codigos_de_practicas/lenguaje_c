/*********************************************************
* Programa: numeros.cc					*
* Objetivo: definicion de la clase Numero		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 05/10/09					*
**********************************************************/
#include "numeros.h"
//-------------------> constructor <----------------

Numero::Numero(int xfibonacci,int xfactorial){
	this->fibonacci=xfibonacci;
	this->factorial=xfactorial;
}

//------------>  serie fibonacci <-----
void Numero::Calculo1(){
	int p,s,f,tope,suma;
	suma=0;
	cout<<"\nIngrese tope de la serie Fibonacci ...:";
	cin>>tope;
	if(tope==1) suma=1;
	else if(tope==2) suma=2;
	else{
		p=s=1;
		suma=2;
		for(int i=0;i<tope-2;i++){
			f=p+s;
			suma+=f;
			p=s;
			s=f;
		}
	}
	this->Fibonacci(suma);
}
//------------> factorial <-----
void Numero::Calculo2(){
	int xvalor,f;
	f=1;
	cout<<"\nIngrese valor a calcular...:";
	cin>>xvalor;
	for(int i=1;i<=xvalor;i++) f*=i;

	this->Factorial(f);
}















