/*********************************************************
* Programa: numeros.h					*
* Objetivo: definicion de la clase Numero		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 05/10/09					*
**********************************************************/
#ifndef _NUMEROS_H
#define _NUMEROS_H
#include <string>
#include <iostream>
using namespace std;

class Numero{
	int fibonacci;
	int factorial;
public:
	Numero(int,int);
	//---------->setters <--------------
	void Fibonacci(int xf){this->fibonacci=xf;}
	int Fibonacci(){return this->fibonacci;}

	void Factorial(int xf){this->factorial=xf;}
	int Factorial(){return this->factorial;}
	//----------> getters <------------


	void Calculo1();
	void Calculo2();
};
#endif










