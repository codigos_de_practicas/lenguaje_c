/************************************************
*Titulo: historia.h				*
*Objetivo: Definicion de la clase cReservacion	*
*Autor: Axelio / axelio8@gmail.com		*
*Fecha: 14/11/09				*
*************************************************/

#ifndef _HISTORIA_H
#define _HISTORIA_H
#include <string>
#include <iostream>
using namespace std;

class cReservacion{
      int vuelo;
      string lugar_dest;
      string lugar_lleg;
      int hora_salida;
      int hora_lleg;
      
public:
      cReservacion();
      cReservacion(int, string, string, int, int);
      
      void Vuelo(int xvuelo){this->vuelo=xvuelo;}
      int Vuelo(){return this->vuelo;}
      
      void Lugar_dest(string xlugar_dest){this->lugar_dest=xlugar_dest;}
      string Lugar_dest(){return this->lugar_dest;}
      
      void Lugar_lleg(string xlugar_lleg){this->lugar_lleg=xlugar_lleg;}
      string Lugar_lleg(){return this->lugar_lleg;}
      
      void Hora_salida(int xhora_salida){this->hora_salida=xhora_salida;}
      int Hora_salida(){return this->hora_salida;}
      
      void Hora_lleg(int xhora_lleg){this->hora_lleg=xhora_lleg;}
      int Hora_lleg(){return this->hora_lleg;}
      
}


#endif