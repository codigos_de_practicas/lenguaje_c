/********************************************************
*Titulo: sec_reservacion.cc				*
*Objetivo: Implementacion de la clase cReservacion	*
*Autor: Axelio / axelio8@gmail.com			*
*Fecha: 14/11/09					*
*********************************************************/

#include "reservacion.h"

      cReservacion::cReservacion(){
	    this->vuelo=0;
	    this->lugar_dest=" ";
	    this->lugar_lleg=" ";
	    this->hora_salida=0;
	    this->hora_lleg=0;
	    
      }
      
      
      cReservacion::cReservacion(int xvuelo, string xlugar_dest, string xlugar_lleg, int xhora_salida, int xhora_lleg){
      
	    this->vuelo=xvuelo;
	    this->lugar_dest=xlugar_dest;
	    this->lugar_lleg=xlugar_lleg;
	    this->hora_salida=xhora_salida;
	    this->hora_lleg=xhora_lleg;
      
      }