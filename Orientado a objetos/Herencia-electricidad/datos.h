/************************************************************************
*Nombre del archivo: datos.h						*
*Descripción: Creación del objeto cDatos				*
*Autor: Axelio / axelio8@gmail.com					*
*Fecha: 13 / 12 / 09 							*
*************************************************************************/

#include "electricidad.h"
#define _ELECTRICIDAD_H

using namespace std;

class cDatos: public cCorpoelec{
      string apellido;
      string nombre;
      int cedula;
      int dia_nac;
      int mes_nac;
      int anio_nac;
public:
      cDatos(string,string,int,int, string,string,int,int,int,int);
      cDatos();
      
      string &Apellido(string xapellido){this->apellido=xapellido;}
      string &Apellido(){return this->apellido;}
      
      string &Nombre(string xnombre){this->nombre=xnombre;}
      string &Nombre(){return this->nombre;}
      
      int &Cedula(int xcedula){this->cedula=xcedula;}
      int &Cedula(){return this->cedula;}
      
      int &Dia_nac(int xdia_nac){this->dia_nac=xdia_nac;}
      int &Dia_nac(){return this->dia_nac;}
      
      int &Mes_nac(int xmes_nac){this->mes_nac=xmes_nac;}
      int &Mes_nac(){return this->mes_nac;}
      
      int &Anio_nac(int xanio_nac){this->anio_nac=xanio_nac;}
      int &Anio_nac(){return this->anio_nac;}
      
};