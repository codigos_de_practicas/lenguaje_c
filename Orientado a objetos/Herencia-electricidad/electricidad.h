/****************************************************************************************
*	Programa: electricidad.h								*	
	Objetivo: Definicion de la clase cBanco						*
*	Autor	: 									*
*	Fecha	: 28/10/09							       	*
*	version: 1.0									*
*	Prueba Practica 								*
*****************************************************************************************/

#ifndef ELECTRICIDAD_H
#define ELECTRICIDAD_H

#include <string>
#include <iostream>
#include<iomanip>
using namespace std;

#include <iomanip>



class cCorpoelec{
	string cuenta;
	string usuario;
	int tipo;
	int kilovat;
	
public:
	cCorpoelec(string,string,int,int);
	cCorpoelec();
	
	string  &Cuenta(){return this->cuenta;}
	string  &Cuenta(string xcuenta){this->cuenta=xcuenta;}

	string  &Usuario(){return this->usuario;}
	string  &Usuario(string xusuario){this->usuario=xusuario;}

	int	&Tipo(){return this->tipo;}
	int	&Tipo(int xtipo){this->tipo=xtipo;}

	int  &Kilovat(){return this->kilovat;}
	int  &Kilovat(int xkilovat){this->kilovat=xkilovat;}

	float Monto_B();
	float Descuento();
	float Iva();
	float Neto();

	//------------------------------------>Sobrecarga

	friend istream &operator >>(istream &,cCorpoelec &);
	friend ostream &operator <<(ostream &,cCorpoelec &);


	// binarios
	
	int operator < (cCorpoelec &);
	int operator > (cCorpoelec &);		


	// aritmetico
	float operator + (cCorpoelec &);
	cCorpoelec operator - (cCorpoelec &);

};
#endif
