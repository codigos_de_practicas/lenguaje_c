/****************************************************************************************
*	Programa: ELECTRICIDAD.cc							*
*	Objetivo: Imprelementacion de la clase cCorpoelec				*
*	Autor	: 									*
*	Fecha	: 26/10/09							       	*
*	version: 1.0									*
*	Prueba Practica 								*
*****************************************************************************************/

#include "electricidad.h"

//------------------------------> Constructor

cCorpoelec::cCorpoelec(string xcuenta,string xusuario,int xtipo,int xkilovat){
	this->cuenta=xcuenta;
	this->usuario=xusuario;
	this->tipo=xtipo;
	this->kilovat=xkilovat;
	
}

cCorpoelec::cCorpoelec(){
	this->cuenta=" ";
	this->usuario=" ";
	this->tipo=0;
	this->kilovat=0;
}
//--------------------------------> Sobrecarga del operador >>

istream &operator >>(istream &entrada,cCorpoelec &objeto){
	string xcuenta,xusuario;
	int xtipo,xkilovat;

	
	cout<<"\n**** INGRESAR DATOS DE LA CUENTA ***\n";

	cout<<"\nNumero de Cuenta......:";entrada>>xcuenta;
	cout<<"\nUsuario...............:";entrada>>xusuario;
	cout<<"\nTipo de Cuenta......:";
	cout<<"\nResidencial......(1):";
	cout<<"\nEmpresarial......(2):";
	cout<<"\nExonerada........(3):";
	do{
		cout<<"\nopcion..:";entrada>>xtipo;
		if(xtipo<1 || xtipo>3){
			cout<<"\nOpcion incorrecta";
			sleep(2);
		}else break;
	}while(true);
	cout<<"\nKilovat consumidos..:";entrada>>xkilovat;
	
	
	
	objeto.Cuenta(xcuenta);
	objeto.Usuario(xusuario);
	objeto.Tipo(xtipo);
	objeto.Kilovat(xkilovat);
	
	return entrada;
}

// -----------> monto basico <------------------
float cCorpoelec::Monto_B(){
	double xprecio;
	if(Tipo()==1) xprecio=1;
	else if(Tipo()==2) xprecio=2.5;
	else xprecio=0.5;
	return (this->Kilovat()*xprecio);
}
// -----------> monto descuento <------------------
float cCorpoelec::Descuento(){
	float xporcentaje;
	if(Tipo()==1) xporcentaje=0.20;
	else if(Tipo()==2) xporcentaje=0.10;
	else xporcentaje==0;
	return (this->Monto_B()* xporcentaje);
}
// -----------> monto iva <------------------
float cCorpoelec::Iva(){
	
	return (double(this->Monto_B()* 0.09));
}
// -----------> monto iva <------------------
float cCorpoelec::Neto(){
	
	return (this->Monto_B()-this->Descuento()+this->Iva());
}

// binariossssssss

int cCorpoelec:: operator < (cCorpoelec &objeto){
	return this->Neto()<objeto.Neto();
}

int cCorpoelec:: operator > (cCorpoelec &objeto){
	return this->Descuento()<objeto.Descuento();
}
// aritmeticosssssssssss

float cCorpoelec:: operator + (cCorpoelec &objeto){
	return this->Neto()+ objeto.Neto();
}

cCorpoelec cCorpoelec::operator-(cCorpoelec &objeto){
	cCorpoelec aux;
	aux.Kilovat(this->Kilovat()-objeto.Kilovat());
	return aux;
}
//--------------------------------> Sobrecarga del operador <<

ostream &operator <<(ostream &salida,cCorpoelec &objeto){
	
	string xmensaje;
	salida<<"\n****  DATOS DE LA CUENTA ***\n";

	salida<<"\nNumero de Cuenta......:"<<objeto.Cuenta();
	salida<<"\nUsuario...............:"<<objeto.Usuario();
	if(objeto.Tipo()==1) xmensaje="Residencial";
	else if(objeto.Tipo()==2) xmensaje="Empresarial";
	else xmensaje="Exonerado";
	salida<<"\nTipo de Cuenta......:"<<xmensaje;
	salida<<"\nKilovat consumidos..:"<<objeto.Kilovat();
	salida<<"\nMonto Básico........:"<<setiosflags(ios::fixed)<<setprecision(2)<<objeto.Monto_B();
	salida<<"\nDescuento......:"<<setiosflags(ios::fixed)<<setprecision(2)<<objeto.Descuento();
	salida<<"\nIva............:"<<setiosflags(ios::fixed)<<setprecision(2)<<objeto.Iva();
	salida<<"\nMonto Neto........:"<<setiosflags(ios::fixed)<<setprecision(2)<<objeto.Neto()<<endl;
	
	
	
	return salida;
}




