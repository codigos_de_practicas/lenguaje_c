/*********************************************************
* Programa: combinatoria.cc					*
* Objetivo: implementacion de la clase Combinatoria		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 14/10/09					*
**********************************************************/
#include "combinatoria.h"

//-------> constructores <-----------------
cCombinatoria::cCombinatoria(){
	this->n=this->p=0;
}
cCombinatoria::cCombinatoria(int xn,int xp){
	this->n=xn;
	this->p=xp;
}

//-------------->  factorial <--------------
int cCombinatoria::Factorial(int xvalor){
	int f=1;
	for(int i=1;i<=xvalor;i++) f*=i;

	return f;
}

//-------------->  factorial <--------------
double cCombinatoria::Combinatoria(){
	int n1,d1,d2;
	n1=this->Factorial(this->N());
	d1=this->Factorial(this->N()-this->P());
	d2=this->Factorial(this->P());
	return (n1/(d1*d2));
	
}

//---------> sobrecarga de entrada <--------------
istream & operator >>(istream &entrada,cCombinatoria &objeto){
	int xn,xp;
	cout<<"\nIngrese valores de la combinatoria";
	cout<<"\nNúmeros de elementos...:";entrada>>xn;
	cout<<"\nNumero de elementos por grupo..:";entrada>>xp;
	objeto.N(xn);
	objeto.P(xp);
	return entrada;
}
//---------> sobrecarga de entrada <--------------
ostream & operator <<(ostream &salida,cCombinatoria &objeto){
	
	cout<<"\nCombinatoria de ("<<objeto.N()<<","<<objeto.P()<<") es...:"<<objeto.Combinatoria()<<endl;

	return salida;
}
















