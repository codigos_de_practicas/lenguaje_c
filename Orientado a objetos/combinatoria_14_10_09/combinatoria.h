/*********************************************************
* Programa: combinatoria.h					*
* Objetivo: definicion de la clase Combinatoria		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 14/10/09					*
**********************************************************/
#ifndef _COMBINATORIA_H
#define _COMBINATORIA_H
#include <string>
#include <iostream>
using namespace std;

class cCombinatoria{
	int n;
	int p;
public:
	cCombinatoria();
	cCombinatoria(int,int);
	// setters y getters
	void N(int xn){this->n=xn;}
	int N(){return this->n;}

	void P(int xp){this->p=xp;}
	int P(){return this->p;}
	// factorial

	//calculos
	int Factorial(int);
	double Combinatoria();
	// sobrecargas de entrada y salida
	friend istream & operator >>(istream &,cCombinatoria &);
	friend ostream & operator <<(ostream &,cCombinatoria &);
};
#endif









