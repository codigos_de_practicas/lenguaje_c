/****************************************************************
* Programa: ferreteria.cc					*
* Objetivo: implementación de la clase Ferreteria				*
* Fecha: 30/09/2009						*
* Autor: jairo molina /www.jairomolina.net.ve/jairo_unerg@hotmail.com
******************************************************************/
#include "ferreteria.h"

//---------------> constructors <----------------------------
Ferreteria::Ferreteria(int xcodigo,string xdescripcion,int xcantidad,double xprecio_c){
	this->codigo=xcodigo;
	this->descripcion=xdescripcion;
	this->cantidad=xcantidad;
	this->precio_c=xprecio_c;
}

//---------------> entrada <----------------------------

void Ferreteria::Entrada(){
	int xcod,xcan;
	string xdes;
	double xprecio_c;

	cout<<"\n*** INGRESE ARTICULOS ***\n";
	cout<<"\nCodigo........:";cin>>xcod;
	cout<<"\nDescripcion...:";cin>>xdes;
	cout<<"\nCantidad......:";cin>>xcan;
	cout<<"\nPrecio Compra.:";cin>>xprecio_c;

	this->Codigo(xcod);
	this->Descripcion(xdes);
	this->Cantidad(xcan);
	this->Precio_c(xprecio_c);
}

//---------------> salida <----------------------------

void Ferreteria::Salida(){

	cout<<"\n*** ARTICULO ***\n";
	cout<<"\nCodigo........:"<<this->Codigo();
	cout<<"\nDescripcion...:"<<this->Descripcion();
	cout<<"\nCantidad......:"<<this->Cantidad();
	cout<<"\nPrecio Compra.:"<<this->Precio_c();
	cout<<"\nInversion.....:"<<this->Inversion();
	cout<<"\nPrecio Venta..:"<<this->Precio_v();
	cout<<"\nGanancia Total:"<<this->Ganancia()<<endl;
}






