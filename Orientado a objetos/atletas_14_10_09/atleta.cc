/*********************************************************
* Programa: atleta.cc					*
* Objetivo: implementacion de la clase Atleta		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 30/09/09					*
**********************************************************/
#include "atleta.h"
//-------->constructor sin paso de parametros <----
Deportista::Deportista(){
	this->cedula=0;
	this->apellido="";
	this->nombre="";
	this->edad=0;
	this->pago_m=0;
}

//-------->constructor por paso de parametros <----
Deportista::Deportista(double xcedula,string xapellido,string xnombre,int xedad,double xpago_m){
	this->cedula=xcedula;
	this->apellido=xapellido;
	this->nombre=xnombre;
	this->edad=xedad;
	this->pago_m=xpago_m;
}

//----------------->mayoria <------------------
string Deportista::Mayoria(){
	if((this->Edad()>=18) && (this->Edad()<50)) return "Aceptable";
	else if (this->Edad()>=50) return "Jubilado";
	else return "No aceptable";
}
//--------> sobrecarga de entrada <------------
istream & operator >>(istream &entrada,Deportista &objeto){
	double xcedula,xpago;
	string xape,xnom;
	int xedad;

	cout<<"\nCedula......:"; entrada>>xcedula;
	cout<<"\nApellido....:"; entrada>>xape;
	cout<<"\nNombre......:"; entrada>>xnom;
	cout<<"\nEdad........:"; entrada>>xedad;
	cout<<"\nMensualidad.:"; entrada>>xpago;

	objeto.Cedula(xcedula);
	objeto.Apellido(xape);
	objeto.Nombre(xnom);
	objeto.Edad(xedad);
	objeto.Pago_m(xpago);
	return entrada;
}

//----------------->mayoria <------------------
ostream & operator <<(ostream &salida,Deportista &objeto){
	salida<<"\nCedula......:"<<objeto.Cedula();
	salida<<"\nApellido....:"<<objeto.Apellido();
	salida<<"\nNombre......:"<<objeto.Nombre();
	salida<<"\nEdad........:"<<objeto.Edad();
	salida<<"\nEstatus.....:"<<objeto.Mayoria();
	salida<<"\nMensualidad.:"<<objeto.Pago_m();
	salida<<"\nIva.........:"<<objeto.Iva();
	salida<<"\nTotal.......:"<<objeto.Total()<<endl;
	return salida;
}
















