
#ifndef _COMBINATORIA_H
#define _COMBINATORIA_H
#include <iostream>
#include <string>
using namespace std;

class cCombinatoria{
	int n;
	int p;

public:
	cCombinatoria();
	cCombinatoria(int,int);

	// SETTERS Y GETTERS 
	
	void N(int xn){this->n=xn;}
	int N(){return this->n;}

	void P(int xp){this->p=xp;}
	int P(){return this->p;}

	//FACTORIAL
	int Factorial(int);
	double Combinatoria(int, int);

	//SOBRECARGA

	friend istream & operator >>(istream &,cCombinatoria &);
	friend ostream & operator <<(ostream &,cCombinatoria &);

};
#endif

