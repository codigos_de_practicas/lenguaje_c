/************************************************************************
* Programa: numeros.cc							*
* Objetivo: aplicar la clase numero					*
* Fecha: 13/10/2008							*
* Autor: jairo MOlina / www.jairomolina.net.ve /jairo_unerg@hotmail.com	*
*************************************************************************/
/* dada una clase que almacene dos numeros, el primero es el resultado de
un numro factorial y el segundo es un valor decimal. En el rograma principal
imprimir el objeto de mayor numero factorial.*/

#include "numero.h"
main(){
	Numero dato1(1,1),dato2(2,2);
	cin>>dato1;
	cin>>dato2;
	if(dato1.Factorial()>dato2.Factorial()) cout<<dato1;
	else cout<<dato2;
}
