/************************************************************************
* Programa: numero.h							*
* Objetivo: definir la clase numero					*
* Fecha: 13/10/2008							*
* Autor: jairo MOlina / www.jairomolina.net.ve /jairo_unerg@hotmail.com	*
*************************************************************************/

#ifndef _NUMERO_H
#define _NUMERO_H
#include <iostream>
using namespace std;

class Numero{
	double factorial;
	float valor;
public:
	Numero(double,float);
	
	// getters y setters

	double &Factorial(){return this->factorial;}
	double &Factorial(double xfactorial){this->factorial=xfactorial;}

	float &Valor(){return this->valor;}
	float &Valor(float xvalor){this->valor=xvalor;}

	//sobrecarga de entrada y salida

	friend istream &operator >>(istream &,Numero &);
	friend ostream &operator <<(ostream &,Numero &);





};
#endif
