/*********************************************************
* Programa: atleta.cc					*
* Objetivo: implementacion de la clase Atleta		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 30/09/09					*
**********************************************************/
#include "atleta.h"
//----------------->constructor <------------------
Deportista::Deportista(double xcedula,string xapellido,string xnombre,int xedad,double xpago_m){
	this->cedula=xcedula;
	this->apellido=xapellido;
	this->nombre=xnombre;
	this->edad=xedad;
	this->pago_m=xpago_m;
}

//----------------->mayoria <------------------
string Deportista::Mayoria(){
	if((this->Edad()>=18) && (this->Edad()<50)) return "Aceptable";
	else if (this->Edad()>=50) return "Jubilado";
	else return "No aceptable";
}
//----------------->mayoria <------------------
int Deportista::Entrada(){
	double xcedula,xpago;
	string xape,xnom;
	int xedad;

	cout<<"\nCedula......:"; cin>>xcedula;
	cout<<"\nApellido....:"; cin>>xape;
	cout<<"\nNombre......:"; cin>>xnom;
	cout<<"\nEdad........:"; cin>>xedad;
	cout<<"\nMensualidad.:"; cin>>xpago;

	this->Cedula(xcedula);
	this->Apellido(xape);
	this->Nombre(xnom);
	this->Edad(xedad);
	this->Pago_m(xpago);

	return 1;
}
//----------------->mayoria <------------------
int Deportista::Salida(){
	cout<<"\nCedula......:"<<this->Cedula();
	cout<<"\nApellido....:"<<this->Apellido();
	cout<<"\nNombre......:"<<this->Nombre();
	cout<<"\nEdad........:"<<this->Edad();
	cout<<"\nEstatus.....:"<<this->Mayoria();
	cout<<"\nMensualidad.:"<<this->Pago_m();
	cout<<"\nIva.........:"<<this->Iva();
	cout<<"\nTotal.......:"<<this->Total()<<endl;
	return 1;
}

