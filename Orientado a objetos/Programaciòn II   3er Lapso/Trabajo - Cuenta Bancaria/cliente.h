#ifndef _CLIENTE_H
#define _CLIENTE_H
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
using namespace std;

struct fn{
	int dia;
	int mes;
	int anio;
};

class cCliente{
	long int cedula;
	string apellidos;
	string nombres;
	fn fecha;
	char sexo;
	string direccion;

public:	
	cCliente();
	
	long int &Cedula(long int xcedula){this->cedula=xcedula;}
	long int &Cedula(){return this->cedula;}
	
	string &Apellidos(string xapellidos){this->apellidos=xapellidos;}
	string &Apellidos(){return this->apellidos;}
	
	string &Nombres(string xnombres){this->nombres=xnombres;}
	string &Nombres(){return this->nombres;}
	
	fn &Fecha(fn xfecha){this->fecha=xfecha;}
	fn &Fecha(){return this->fecha;}
	
	char &Sexo(int xsexo){this->sexo=xsexo;}
	char &Sexo(){return this->sexo;}
	
	string &Direccion(string xdireccion){this->direccion=xdireccion;}
	string &Direccion(){return this->direccion;}
	
	int &Dia(){return this->fecha.dia;}
	int &Mes(){return this->fecha.mes;}
	int &Anio(){return this->fecha.anio;}
	
	int Edad();
	
	
	
	//sobrecarga de entrada y salida
	friend ostream & operator <<(ostream &,cCliente&);
	friend istream & operator >>(istream &,cCliente&);
	
}; 
#endif
