/************************************************************************
*Nombre del Programa: cliente.cc					*
*Descripción:Implementación de la clase cliente.h			*
*Autores: Jennifer Montilla y Axel Díaz					*
*Correos: montilla.jennifer@gmail.com / axelio_8@.com*
*Fecha: 27 de Enero de 2010						*
*************************************************************************/

#include "cliente.h"
using namespace std;

cCliente::cCliente(){
	this->cedula=0;
	this->apellidos=" ";
	this->nombres=" ";
 	this->fecha.dia=0;
 	this->fecha.mes=0;
 	this->fecha.anio=0;
	this->sexo=0;
	this->direccion=" ";
}


istream & operator >>(istream &entrada,cCliente &puntero){
	
	time_t tSac = time(NULL);
	tm tms = *localtime(&tSac);
	
	fn hoy;
	int xa=0;
	hoy.dia=tms.tm_mday;
	hoy.mes=tms.tm_mon+1;
	hoy.anio=tms.tm_year+1900;
	
	string xapellidos, xnombres, xdireccion;
	int xcedula;
 	fn xfecha;
	char xsexo;
	
	cout<<endl;cout.width(78);cout<<"DATOS PERSONALES";cout<<endl;
	cout<<endl;cout.width(80);cout<<"Cedula................: ";entrada>>xcedula;
	cout<<endl;cout.width(80);cout<<"Apellido..............: ";entrada>>xapellidos;
	cout<<endl;cout.width(80);cout<<"Nombre................: ";entrada>>xnombres;
	cout<<endl;cout.width(81);cout<<"Fecha de Nacimiento...: \n";
	cout<<endl;cout.width(68);cout<<"Año: ";entrada>>xfecha.anio;
	while(xfecha.anio>hoy.anio){
		sleep(1);
		cout<<"\t\a\n¡ERROR!";
		cout<<"\nUsted no ha nacido, intente de nuevo...\n";
		entrada>>xfecha.anio;
	}
	
	
	
	cout<<endl;cout.width(67);cout<<"Mes: ";entrada>>xfecha.mes;
	
	if(xfecha.anio==hoy.anio){
		while(xfecha.mes>hoy.mes){
			sleep(1);
			cout<<"\t\a\n¡ERROR!";
			cout<<"\nUsted no ha nacido, intente de nuevo...\n";
			entrada>>xfecha.mes;
		}
	}
	
	while(xfecha.mes>12){
		
			sleep(1);
			cout<<"\t\a\n¡ERROR!";
			cout<<"\nMes incorrecto, por favor intente de nuevo...\n";
			entrada>>xfecha.mes;
		
	}
	
	
	
	cout<<endl;cout.width(67);cout<<"Dia: ";entrada>>xfecha.dia;
	
	if((xfecha.anio==hoy.anio)||(xfecha.mes==hoy.mes)){
		while(xfecha.dia>hoy.dia){
			sleep(1);
			cout<<"\t\a\n¡ERROR!";
			cout<<"\nUsted no ha nacido, intente de nuevo...\n";
			entrada>>xfecha.dia;
		}
	}
	
	if((xfecha.mes==1)||(xfecha.mes==3)||(xfecha.mes==5)||(xfecha.mes==7)||(xfecha.mes==8)||(xfecha.mes==10)||(xfecha.mes==12)){
		while(xfecha.dia>31){
			
				sleep(1);
				cout<<"\t\a\n¡ERROR!";
				cout<<"\nDia incorrecto, por favor intente de nuevo...\n\n";
				entrada>>xfecha.dia;
			
		}
		
	} else if ((xfecha.mes==4)||(xfecha.mes==6)||(xfecha.mes==9)||(xfecha.mes==11)){
		while(xfecha.dia>30){
		
				sleep(1);
				cout<<"\a\n¡ERROR!";
				cout<<"\nDia incorrecto, por favor intente de nuevo...\n\n";
				entrada>>xfecha.dia;
			
		}
	}
		

	cout<<endl;cout.width(83);cout<<"Direccion................: ";entrada>>xdireccion;	
	cout<<endl;cout.width(83);cout<<"Sexo (F/M)...............: ";entrada>>xsexo;
	if(xsexo!='f'&&xsexo!='F'&&xsexo!='M'&&xsexo!='m'){
	do{
		sleep(1);
		cout<<"\t\a\n¡ERROR!";
		cout<<"\nOpcion incorrecta, por favor intente de nuevo...\n\n";
		entrada>>xsexo;
	}while(xsexo!='f'&&xsexo!='F'&&xsexo!='M'&&xsexo!='m');
	}
	

	
	puntero.Cedula(xcedula);
	puntero.Apellidos(xapellidos);
	puntero.Nombres(xnombres);
	puntero.Fecha(xfecha);
	puntero.Sexo(xsexo);
	puntero.Direccion(xdireccion);
	
	return entrada;
}


ostream & operator <<(ostream &salida,cCliente &puntero){
	
	salida<<"\nDatos Personales de "<<puntero.nombres<<" "<<puntero.apellidos;
	salida<<"\n\nCedula...................: "<<puntero.cedula;
	salida<<"\nApellido.................: "<<puntero.apellidos;
	salida<<"\nNombre...................: "<<puntero.nombres;
	salida<<"\nDireccion................: "<<puntero.direccion;	
	salida<<"\nSexo.....................: ";
	if(puntero.sexo=='M'||puntero.sexo=='m'){
		salida<<"Masculino";
	}else salida<<"Femenino";
	salida<<"\nEdad.....................: "<<puntero.Edad();
	
	return salida;
}

int cCliente::Edad(){
	
	time_t tSac = time(NULL);
	tm tms = *localtime(&tSac);
	
	fn hoy;
	int xa=0;
	hoy.dia=tms.tm_mday;
	hoy.mes=tms.tm_mon+1;
	hoy.anio=tms.tm_year+1900;
	
	xa=hoy.anio-fecha.anio;
	
	if(hoy.mes==fecha.mes){
		if(hoy.dia<fecha.dia){
			xa--;
		}
	}else if(hoy.mes<fecha.mes){
		xa--;
		}
	
	return xa;

}	

void cCliente::Buscar(){
int opcion, xcedula;
cout<<"Menú Buscar\n";
cout<<"\n1. Buscar por Cedula de Identidad";
cout<<"\n2. Buscar por ";
cout<<"\n3. Buscar por ";
cout<<"\n4. Salir";
cin>>opcion;

	switch(opcion){
		case 1:
			cout<<"Introduzca la cédula que quiere buscar: ";
			cin>>xcedula;
			
			if(xcedula==this->cedula){
				cout<<"CEDULA ENCONTRADA";
			}else{cout<<"CEDULA NO ENCONTRADA";}
			
		break;
		
		case 2:
		break;
		
		case 3:
		break;
		
		case 4;
		break;
	
	}
}