#ifndef _TRANSACCION_H
#define _TRANSACCION_H
#include "cliente.h"

using namespace std;

struct ft{
	int dia;
	int mes;
	int anio;
};

class cTrans:public cCliente{
	long int nro_cuenta;
	int tipo_trans;
	double monto;
	ft fecha_trans;
	
public:
	cTrans();
	
	long int &Nro_cuenta(long int xnro_cuenta){this->nro_cuenta=xnro_cuenta;}
	long int &Nro_cuenta(){return this->nro_cuenta;}
	
	int &Tipo_trans(int xtipo_trans){this->tipo_trans=xtipo_trans;}
	int &Tipo_trans(){return this->tipo_trans;}
	
	double &Monto(double xmonto){this->monto=xmonto;}
	double &Monto(){return this->monto;}
	
	ft &Fecha_trans(ft xfecha_trans){this->fecha_trans=xfecha_trans;}
	ft &Fecha_trans(){return this->fecha_trans;}
	
	//sobrecarga de entrada y salida
	friend ostream & operator <<(ostream &,cTrans&);
	friend istream & operator >>(istream &,cTrans&);

};
#endif