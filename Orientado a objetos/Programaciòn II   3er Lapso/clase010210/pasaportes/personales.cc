/************************************************************************************************
* Programa: personales.cc									*
* Objetivo: implementar la clase personales							*
* Fecha: 19/01/2010										*
* Autor: jairo molina 										*
* www.jairomolina.net.ve/jairo_unerg@hotmail.com						*	************************************************************************************************/

#include "personales.h"

//********************** constructor ***************************************************
cPersonales::cPersonales(){
	this->cedula=0;
	
	for (int i=0; i<20; i++) this->apellidos[i]=0;
	for (int i=0; i<20; i++) this->nombres[i]=0;
	
	this->fnac.dia=0;
	this->fnac.mes=0;
	this->fnac.anio=0;
	this->estado=' ';

}

//*****************************Calculo de EDad ********************************************
int cPersonales::Edad(){
	fecha hoy;
	int xe;
	hoy.dia=1;
	hoy.mes=2;
	hoy.anio=2010;

	xe=hoy.anio-fnac.anio;
	


	if(this->fnac.mes>hoy.mes) return xe-1;
	else if(this->fnac.mes==hoy.mes){
		if(this->fnac.dia<=hoy.dia) return xe;
		else return xe-1;
		}
	
	return xe;
}

//************ convierte en mayuscula frase *********
void Convertir(char* cadena){
	while(*cadena !='\0'){
		*cadena=toupper(*cadena);
		++ cadena;
	}
}

//****************************** sobrecarga de entrada ************************************
void cPersonales::Leer(long int xcedula){
	
	char xapellidos[20],xnombres[20];
	for (int i=0; i<20; i++) xapellidos[i]=0;
	for (int i=0; i<20; i++) xnombres[i]=0;
	fecha xfnac;
	char xestado;
	system("clear");
	cout<<endl;cout.width(50);cout<<"******************************";
	cout<<endl;cout.width(50);cout<<"** INGRESE DATOS PERSONALES **";
	cout<<endl;cout.width(50);cout<<"******************************";
		
	cout<<endl;cout.width(45);cout<<"Cedula................:"<<xcedula<<"\n";
	cout<<endl;cout.width(45);cout<<"Apellidos.............:";cin>>xapellidos;
	cout<<endl;cout.width(45);cout<<"Nombres...............:";cin>>xnombres;
	cout<<endl;cout.width(45);cout<<"Fecha de Nacimiento...:";
	cout<<endl;cout.width(45);cout<<"Dia........:";cin>>xfnac.dia;
	cout<<endl;cout.width(45);cout<<"Mes........:";cin>>xfnac.mes;
	cout<<endl;cout.width(45);cout<<"Año........:";cin>>xfnac.anio;
	xestado='A';
	/*this->Convertir(xapellidos);
	this->Convertir(xnombres);*/
	this->Cedula(xcedula);
	this->Apellidos(xapellidos);
	this->Nombres(xnombres);
	this->Fnac(xfnac);
	this->Estado(xestado);

}
//****************************** sobrecarga de entrada ************************************
ostream & operator <<(ostream &salida,cPersonales &objeto){
	

	cout<<endl<<left;cout.width(10);cout<<objeto.Cedula();
	cout.width(15);cout<<objeto.Apellidos();
	cout.width(15);cout<<objeto.Nombres();
	cout.width(15);cout<<objeto.fnac.dia<<"/"<<objeto.fnac.mes<<"/"<<objeto.fnac.anio;
	cout.width(10);cout<<objeto.Edad();
	cout<<endl;

	return salida;
}

void cPersonales::Cabeza(int xpag,char xestado){
	system("reset");
	char* xmen;
	if(xestado=='A') xmen="** REPORTE DE PERSONAS ACTIVAS  **";
	else xmen="** REPORTE DE PERSONAS DESACTIVADAS  **";
	cout<<endl;cout.width(55);cout<<"**************************************";
	cout<<endl;cout.width(55);cout<<xmen;
	cout<<endl;cout.width(55);cout<<"**************************************";
	cout<<endl;cout.width(55);cout<<"Pagina...:"<<xpag;	
	cout<<endl<<endl;cout.width(10);cout<<"CEDULA";
	cout.width(15);cout<<"APELLIDOS";
	cout.width(15);cout<<"NOMBRES";
	cout.width(23);cout<<"FECHA NAC";
	cout.width(11);cout<<"EDAD";
	cout<<endl;cout.width(15);cout<<"---------------------------------------------------------------------------";
	
}
