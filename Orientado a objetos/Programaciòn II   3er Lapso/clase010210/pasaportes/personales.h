/************************************************************************************************
* Programa: personales.h									*
* Objetivo: definir la clase personales								*
* Fecha: 19/01/2010										*
* Autor: jairo molina 										*
* www.jairomolina.net.ve/jairo_unerg@hotmail.com
*************************************************************************************************/
#ifndef _PERSONALES_H
#define _PERSONALES_H

#include <iostream>
#include <string>
using namespace std;

struct fecha{
	int dia;
	int mes;
	int anio;
};

class cPersonales{
	long int cedula;
	char apellidos[20];
	char nombres[20];
	fecha fnac;
	char estado;
public:
	cPersonales();

	long int &Cedula(long int xcedula){this->cedula=xcedula;}
	long int &Cedula(){return this->cedula;}
	
	char* Apellidos(char* xapellidos){strcpy(this->apellidos,xapellidos);}
	char* Apellidos(){return this->apellidos;}

	char* Nombres(char* xnombres){strcpy(this->nombres,xnombres);}
	char* Nombres(){return this->nombres;}

	fecha &Fnac(fecha xfnac){this->fnac=xfnac;}
	fecha &Fnac(){return this->fnac;}
	
	int &Dia(){return this->fnac.dia;}
	int &Mes(){return this->fnac.mes;}
	int &Anio(){return this->fnac.anio;}

	char &Estado(char xestado){this->estado=xestado;}
	char &Estado(){return this->estado;}

	int Edad();

	void Leer(long int);
	void Cabeza(int,char);
	void Convertir(char*);
	
	// sobrecarga
	
	friend ostream & operator <<(ostream &,cPersonales &);

};
#endif
