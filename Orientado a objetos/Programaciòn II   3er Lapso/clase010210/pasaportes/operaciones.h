//////////////////////////////////////////////////////////////////7
// buscar un registro especifico en el archivo			//
/////////////////////////////////////////////////////////////////
int Buscar(long int xcedula,char *xarchivo,cPersonales &registro,int ver){
	ifstream archivo(xarchivo,ios::in|ios::binary);// abre el archivo de tipo lectura
	archivo.read((char*)&registro,sizeof(cPersonales)); // primera lectura
	while (archivo && !archivo.eof()){
		if(xcedula==registro.Cedula()){
			archivo.close();
			 return 1;
		}
		archivo.read(reinterpret_cast<char*>(&registro),sizeof(cPersonales));// continuar leyendo
	}
	archivo.close();
	return 0;
}
//////////////////////////////////////////////////////////////////7
// Almacenar un registro en el archivo				//
/////////////////////////////////////////////////////////////////
void Registrar(cPersonales &registro,cArchivo &data,char* xarchivo){
	
	int xcedula;
	system("clear");
	Mensaje("*      REGISTRAR  DATOS      *");
	char xmen;

	

	cout<<endl;cout.width(45);cout<<"Ingrese Cedula....:"; cin>>xcedula;
	if(!Buscar(xcedula,"datos.dat",registro,0)){
		registro.Leer(xcedula);
		data.Grabar(registro,xarchivo);
		
	}
	else{
		if(registro.Estado()=='A') registro.Cabeza(1,'A');
		else registro.Cabeza(1,'R');		
		cout<<registro;
					
		cout<<endl<<endl;
		Mensaje("*  CEDULA   YA    REGISTRADA *");
		Pausa();
		
	}
	
}
//////////////////////////////////////////////////////////////////7
// Almacenar un registro en el archivo				//
/////////////////////////////////////////////////////////////////
void Encontrar(cPersonales &registro,cArchivo &data,char* xarchivo){
	
	int xcedula;
	system("clear");
	Mensaje("*      BUSCAR  REGISTRO      *");
	
	cout<<endl;cout.width(45);cout<<"Ingrese Cedula....:"; cin>>xcedula;
	if(!Buscar(xcedula,"datos.dat",registro,0)){
		Mensaje("*  CEDULA   NO    REGISTRADA *");
		Pausa();
	}
	else{
		if(registro.Estado()=='A') registro.Cabeza(1,'A');
		else registro.Cabeza(1,'R');		
		cout<<registro;
		cout<<endl<<endl;
		Pausa();
		
	}
}

void MenuReportes(cArchivo &dato){
	int xopcion;
	

	do{
		system("clear");
		Mensaje("* M E N U    R E P O R T E S *");
		
		cout<<endl;cout.width(45);cout<<"Activos ........(1)";
		cout<<endl;cout.width(45);cout<<"Desactivados....(2)";
		cout<<endl;cout.width(45);cout<<"Retornar........(3)";
		cout<<endl<<endl<<endl;cout.width(30);cout<<"Ingrese Opcion.....:"; cin>>xopcion;
		switch (xopcion){
		case 1:
			dato.Mostrar("datos.dat",'A');
			Pausa();
		
		break;
		case 2:
			dato.Mostrar("datos.dat",'R');	
			Pausa();		
		break;
		case 3:
			//retorna al sistema 
			
		break;
		default:
			system("clear");
			Mensaje("*O P C I O N  I N V A L I D A*");
			Pausa();
			xopcion=1;
		break;
		}
	}while(xopcion!=3);


}


