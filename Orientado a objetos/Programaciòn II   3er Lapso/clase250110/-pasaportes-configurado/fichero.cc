/****************************************************************************************
* Programa: fichero.h									*
* Objetivo: definir la clase del archivo						*
* Fecha: 19/01/2010									*
* Autor: jairo molina 									*
* www.jairomolina.net.ve/jairo_unerg@hotmail.com					*
*****************************************************************************************/

#include "fichero.h"

//-------------------------------->implementacion 

//////////////////////////////////////////////////////////////////////
// Crear archivo
////////////////////////////////////////////////////////////////////////

int cArchivo::Crear (char * xarchivo){
	int xclave,intentos=0;
	
	do{
		cout<<"\nIngrese clave de seguridad...:";cin>>xclave;
		if(xclave==12345){


			fstream archivo(xarchivo,ios::out|ios::binary);

			cout.width(50);cout<<"*********************************"<<endl;
			cout.width(50);cout<<"*   Archivo Creado con Exito    *"<<endl;
			cout.width(50);cout<<"*********************************"<<endl;
		
			Pausa();
		
			return 1;
		}else{
			cout.width(50);cout<<"*********************************"<<endl;
			cout.width(50);cout<<"*   Clave Incorrecta   *"<<endl;
			cout.width(50);cout<<"*********************************"<<endl;
			intentos++;
		}
	}while(intentos<3);
	cout.width(50);cout<<"*********************************"<<endl;
	cout.width(50);cout<<"*   Se acabo el Numero de Intentos   *"<<endl;
	cout.width(50);cout<<"*********************************"<<endl;			
	return 0;
}


//////////////////////////////////////////////////////////////////////
// Grabar datos en  archivo
////////////////////////////////////////////////////////////////////////

int cArchivo::Grabar(cPersonales &registro, char* xarchivo){

	ofstream archivo(xarchivo,ios::app|ios::binary);
	
	archivo.write((char*)&registro,sizeof(cPersonales));


	archivo.close();cout<<endl<<endl;
	
	cout.width(30);cout<<"*********************************"<<endl;
	cout.width(30);cout<<"*  Carajo  Almacenado con Exito   *"<<endl;
	cout.width(30);cout<<"*********************************"<<endl;

	Pausa();

	return 1;
}




