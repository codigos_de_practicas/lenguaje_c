/************************************************************************************************
* Programa: personales.h									*
* Objetivo: definir la clase personales								*
* Fecha: 19/01/2010										*
* Autor: jairo molina 										*
* www.jairomolina.net.ve/jairo_unerg@hotmail.com
*************************************************************************************************/
#ifndef _PERSONALES_H
#define _PERSONALES_H

#include <iostream>
#include <string>
using namespace std;

struct fecha{
	int dia;
	int mes;
	int anio;
};

class cPersonales{
	long int cedula;
	string apellidos;
	string nombres;
	fecha fnac;
	char estado;
public:
	cPersonales();
	
	long int &Cedula(long int xcedula){this->cedula=xcedula;}
	long int &Cedula(){return this->cedula;}
	
	string &Apellidos(string xapellidos){this->apellidos=xapellidos;}
	string &Apellidos(){return this->apellidos;}

	string &Nombres(string xnombres){this->nombres=xnombres;}
	string &Nombres(){return this->nombres;}

	fecha &Fnac(fecha xfnac){this->fnac=xfnac;}
	fecha &Fnac(){return this->fnac;}
	

	char &Estado(char xestado){this->estado=xestado;}
	char &Estado(){return this->estado;}

	int Edad();

	void Leer(long int);
	void Cabeza();
	
	// sobrecarga
	
	friend ostream & operator <<(ostream &,cPersonales &);

};
#endif
