int Buscar(long int xcedula,char *xarchivo,cPersonales &registro,int ver){
	
	ifstream archivo(xarchivo,ios::in|ios::binary);
	
	archivo.read((char*)&registro,sizeof(cPersonales)); // primera lectura

	while (archivo && !archivo.eof()){
		if(xcedula==registro.Cedula()) return 1;
		archivo.read(reinterpret_cast<char*>(&registro),sizeof(cPersonales));// continuar leyendo
	}
	archivo.close();
	return 0;
}

void Registrar(cPersonales &registro,cArchivo &data,char* xarchivo){
	
	int xcedula;
	system("clear");
	Mensaje("*      REGISTRAR  DATOS      *");
	
	cout<<endl;cout.width(45);cout<<"Ingrese Cedula....:"; cin>>xcedula;
	if(!Buscar(xcedula,"datos.dat",registro,0)){
		registro.Leer(xcedula);
		data.Grabar(registro,xarchivo);
		
	}
	else{
		Mensaje("*  CEDULA   YA    REGISTRADA *");
		
		Pausa();
		
	}
	
}
