/****************************************************************************************
* Programa: fichero.h									*
* Objetivo: definir la clase del archivo						*
* Fecha: 19/01/2010									*
* Autor: jairo molina 									*
* www.jairomolina.net.ve/jairo_unerg@hotmail.com					*
*****************************************************************************************/
#ifndef FICHERO_H
#define FICHERO_H
#include <iostream>
#include <string>
using namespace std;
#include <fstream>
using namespace std;


class cArchivo:public fstream{ // clase Cdatos , maneja el archivo

public:
	int Existe(char*);
	int Crear(char*);
	int Grabar(cPersonales &, char*);

	
};


//-------------------------------->implementacion 
//////////////////////////////////////////////////////////////////////
// Verifica si existe el archivo
////////////////////////////////////////////////////////////////////////

int cArchivo::Existe(char* xarchivo){

	ifstream archivo(xarchivo,ios::in|ios::binary);

	if (archivo){
		archivo.close();
	 	return 1;
	}
	else return 0;
}
//////////////////////////////////////////////////////////////////////
// Crear archivo
////////////////////////////////////////////////////////////////////////

int cArchivo::Crear (char * xarchivo){
	int xclave,intentos=0,exis=0;
	char pre;
	
	if(this->Existe(xarchivo)){
		Mensaje("Archivo ya Existe y se borrara");
		exis=1;
	}
	cout<<"\n";
	cout.width(45);cout<<"Desea Crearlo...(S/N):";cin>>pre;
	pre=toupper(pre);
	if(pre=='S'){
		do{
			cout<<endl;
			cout.width(50);cout<<"Ingrese clave de seguridad...:";cin>>xclave;
			if(xclave==12345){
				fstream archivo(xarchivo,ios::out|ios::binary);
				Mensaje("* Archivo Creado con Exito *");
				Pausa();
				return 1;
			}else{
				Mensaje("*   Clave Incorrecta   *");
				intentos++;
			}
		}while(intentos<3);
		Mensaje("*   Se acabo el Numero de Intentos   *");
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////
// Grabar datos en  archivo
////////////////////////////////////////////////////////////////////////

int cArchivo::Grabar(cPersonales &registro, char* xarchivo){

	ofstream archivo(xarchivo,ios::app|ios::binary);
	
	archivo.write((char*)&registro,sizeof(cPersonales));


	archivo.close();cout<<endl<<endl;
	
	Mensaje("Cedula Registrada con Exito");

	Pausa();

	return 1;
}
//////////////////////////////////////////////////////////////////////
// buscar datos en  archivo
////////////////////////////////////////////////////////////////////////
/*
//--------------------------> Funcion Buscar <-------------------------
int Buscar(long int xcedula,char *xarchivo,cPersonales &registro,int ver){
	
	ifstream archivo(xarchivo,ios::in|ios::binary);
	
	archivo.read((char*)&registro,sizeof(cPersonales)); // primera lectura

	while (archivo && !archivo.eof()){
		if(xcedula==registro.Cedula()==0) return 1;
		archivo.read(reinterpret_cast<char*>(&registro),sizeof(cPersonales));// continuar leyendo
	}
	archivo.close();
	return 0;
}
*/
#endif

