/************************************************************************************************
* Programa: personales.cc									*
* Objetivo: implementar la clase personales							*
* Fecha: 19/01/2010										*
* Autor: jairo molina 										*
* www.jairomolina.net.ve/jairo_unerg@hotmail.com						*	************************************************************************************************/

#include "personales.h"


//********************** constructor ***************************************************

cPersonales::cPersonales(){
	this->cedula=0;
	this->apellidos="";
	this->nombres="";
	this->fnac.dia=0;
	this->fnac.mes=0;
	this->fnac.anio=0;
	this->estado=' ';

}

//*****************************Calculo de EDad ********************************************
int cPersonales::Edad(){
	fecha hoy;
	int xe;
	hoy.dia=20;
	hoy.mes=1;
	hoy.anio=2010;

	xe=hoy.anio-fnac.anio;

	if(this->fnac.mes>hoy.mes) return xe-1;
	else if(this->fnac.mes=hoy.mes){
		if(this->fnac.dia<=hoy.dia) return xe;
		else return xe-1;
		}
} 

//****************************** sobrecarga de entrada ************************************
void cPersonales::Leer(long int xcedula){
	
	string xapellidos,xnombres;
	fecha xfnac;
	char xestado;
	
	cout<<"\n***** INGRESE DATOS PERSONALES *****\n";

	cout<<"\nCedula................:"<<xcedula;
	cout<<"\nApellidos.............:";cin>>xapellidos;
	cout<<"\nNombres...............:";cin>>xnombres;
	cout<<"\nFecha de Nacimiento...:";
	cout<<"\nDia...................:";cin>>xfnac.dia;
	cout<<"\nMes...................:";cin>>xfnac.mes;
	cout<<"\nAño...................:";cin>>xfnac.anio;
	xestado='A';
	this->Cedula(xcedula);
	this->Apellidos(xapellidos);
	this->Nombres(xnombres);
	this->Fnac(xfnac);
	this->Estado(xestado);
	 

	
}
//****************************** sobrecarga de entrada ************************************
ostream & operator <<(ostream &salida,cPersonales &objeto){
	
	
	salida<<"\n***** DATOS PERSONALES *****\n";

	salida<<"\nCedula................:"<<objeto.Cedula();
	salida<<"\nApellidos.............:"<<objeto.Apellidos();
	salida<<"\nNombres...............:"<<objeto.Nombres();
	salida<<"\nEdad..................:"<<objeto.Edad();
	 

	return salida;
}
