/********************************************************************************
*Nombre: estudiantessec.cc								*
*Descripción:	Implementacion de la clase cEstudiante				*
*Autor:	Axel Díaz / axelio8@gmail.com						*
*Fecha: 02/11/09								*
*********************************************************************************/

#include "estudiantes.h"
#include <stdlib.h>
#include <time.h>


cEstudiante::cEstudiante(string xnombre,string xapellido,int xcedula, float xnota1, float xnota2, float xnota3){
      this->nombre=xnombre;
      this->apellido=xapellido;
      this->cedula=xcedula;
      this->nota1=xnota1;
      this->nota2=xnota2;
      this->nota3=xnota3;
}

cEstudiante::cEstudiante(){
      this->nombre="";
      this->apellido="";
      this->cedula=0;
      this->nota1=0;
      this->nota2=0;
      this->nota3=0;
}

float eNota(float xnota){
      
      while(xnota<0){
	    if (xnota<0){
		  cout<<"La nota debe ser mayor de 0, intente de nuevo..."; 
		  cout<<"\nIntroduzca nota: ";
		  xnota=0;
		  cin>>xnota;}
		  eNota(xnota);
      }
      while(xnota>100){
	    if (xnota>100){
		  cout<<"La nota debe ser menor de 100, intente de nuevo..."; 
		  cout<<"\nIntroduzca nota: ";
		  xnota=0;
		  cin>>xnota;}
		  eNota(xnota);
      }
      
      return xnota;
}


//Sobrecarga de Entrada
istream & operator >>(istream&leer, cEstudiante&puntero){

      string xnombre, xapellido;
      int xcedula, xedad;
      float xnota1, xnota2, xnota3;
      srand(time(NULL));
      
      cout<<"\nNombre.....: "; leer>>xnombre;
      cout<<"\nApellido...: "; leer>>xapellido;
      cout<<"\nCedula.....: "; leer>>xcedula;//=1+rand()%9;
      cout<<"\nNota 1.....: "; leer>>xnota1;//=/and()%100;
      eNota(xnota1);
      cout<<"\nNota 2.....: "; leer>>xnota2;//=rand()%100;
      eNota(xnota2);
      cout<<"\nNota 3.....: "; leer>>xnota3;//=rand()%100;
      eNota(xnota3);

      
      puntero.Nombre(xnombre);
      puntero.Apellido(xapellido);
      puntero.Cedula(xcedula);
      puntero.Nota1(xnota1);
      puntero.Nota2(xnota2);
      puntero.Nota3(xnota3);

      return leer;
}

//Sobrecarga de Salida
ostream & operator <<(ostream&imprimir, cEstudiante&puntero){
      
      imprimir<<"\nNombre.........: "<<puntero.Nombre();
      imprimir<<"\nApellido.......: "<<puntero.Apellido();
      imprimir<<"\nCedula.........: "<<puntero.Cedula();
      imprimir<<"\nPrimera nota...: "<<puntero.Nota1();
      imprimir<<"\nSegunda nota...: "<<puntero.Nota2();
      imprimir<<"\nTercera nota...: "<<puntero.Nota3();
      imprimir<<"\nPromedio.......: "<<puntero.Promedio();
      imprimir<<"\nStatus.........: ";
      if(puntero.Aprobado()==1) cout<<"Aprobado";
      if(puntero.Aprobado()==0) cout<<"Reprobado";
      
      imprimir<<endl;
      
      return imprimir;
}

//Sobrecarga de Operadores Binarios (que comparan dos valores)
double cEstudiante::operator < (cEstudiante&puntero){
      return this->Promedio()<puntero.Promedio();
}

//Sobrecarga de Operadores Aritméticos (que suman, mult, div, etc dos valores)
double cEstudiante::operator + (cEstudiante&puntero){
      return this->Promedio() + puntero.Promedio();
      
}
