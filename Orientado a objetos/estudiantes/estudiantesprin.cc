/********************************************************************************
*Nombre: estudiantesprin.cc							*
*Descripción:	aplicacion de la clase cEstudiante				*
*Autor:	Axel Díaz / axelio8@gmail.com						*
*Fecha: 02/11/09								*
*********************************************************************************/

#include "estudiantes.h"
#define CANT 2

main(){
      cEstudiante alumno[CANT];
      
      for(int i=0;i<2;i++){
	    cin>>alumno[i];
      }
      
      
      
      cout<<"\n\n\nImpresiones...\n\n";
      
      if(alumno[0]<alumno[1]){
	    cout<<alumno[0];
	    sleep(3);
	    cout<<alumno[1];
      }else{cout<<alumno[1];
	    sleep(3);
	    cout<<alumno[0];
      }
      
      cout<<"\nEl promedio del curso es: ";
      
      cout<<(alumno[0])+(alumno[1]);
      
      
}
