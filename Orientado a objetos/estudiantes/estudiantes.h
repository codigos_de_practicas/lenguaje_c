/********************************************************************************
*Nombre: estudiantes.h								*
*Descripción:	Creación de la clase cEstudiante				*
*Autor:	Axel Díaz / axelio8@gmail.com						*
*Fecha: 02/11/09								*
*********************************************************************************/
#ifndef _ESTUDIANTES_H
#define _ESTUDIANTES_H
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

class cEstudiante{
      string nombre;
      string apellido;
      int cedula;
      float nota1;
      float nota2;
      float nota3;
      
public:
      cEstudiante();
      cEstudiante(string, string, int, float, float, float);

      //Setters y Getters
      
      void Nombre(string xnombre){this->nombre=xnombre;}
      string Nombre(){return this->nombre;}
      
      void Apellido(string xapellido){this->apellido=xapellido;}
      string Apellido(){return this->apellido;}
      
      void Cedula(int xcedula){this->cedula=xcedula;}
      int Cedula(){return this->cedula;}
      
      void Nota1(float xnota1){this->nota1=xnota1;}
      float Nota1(){return this->nota1;}
      
      void Nota2(float xnota2){this->nota2=xnota2;}
      float Nota2(){return this->nota2;}
      
      void Nota3(float xnota3){this->nota3=xnota3;}
      float Nota3(){return this->nota3;}
      
      double Promedio(){
	    double promedio;
	    promedio = (Nota1()+Nota2()+Nota3())/3;
	    return promedio;
      }
      
      int Aprobado(){
	    if(Promedio()>55) return 1;
	    else return 0;
	    }
      
      
      
      //Sobrecarga de entrada
      friend istream & operator >>(istream &, cEstudiante &);
      
      //Sobrecarga de salida
      friend ostream & operator <<(ostream &, cEstudiante &);
      
      //Sobrecarga de Binarios
      double operator < (cEstudiante &);
};
      
      //Sobrecarga de Aritméticos
      double operator + (cEstudiante &);


#endif
