/************************************************
*Nombre: Matematica.h				*
*Objetivo: Definicion de la Clase Saludo.h	*
*Autor: Axelio / axelio8@gmail.com		*
*Fecha: 17/10/09				*
*************************************************/

//{}
#ifndef _MATEMATICA_H
#define _MATEMATICA_H
#include <iostream>
using namespace std;

class Matematico{

int Suma;
int Multiplicacion;

public:

Matematica(int, int);
  
//setters y getters
int Suma(int xSuma){this->Suma=xSuma;}
int Suma(){return this->Suma;}


int Multiplicacion(int xMultiplicacion){this->Multiplicacion=xMultiplicacion;}
int Multiplicacion(){return this->Multiplicacion;}

  };


#endif
