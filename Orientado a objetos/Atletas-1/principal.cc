/*********************************************************
* Programa: atleta.cc					*
* Objetivo: implementacion de la clase Atleta		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 30/09/09					*
**********************************************************/
#include "atleta.h"

main(){
	Deportista atleta1(0,"","",0);
	
	double xcedula;
	string xape,xnom;
	int xedad;

	cout<<"\nCedula...:"; cin>>xcedula;
	cout<<"\nApellido.:"; cin>>xape;
	cout<<"\nNombre...:"; cin>>xnom;
	cout<<"\nEdad.....:"; cin>>xedad;

	atleta1.Cedula(xcedula);
	atleta1.Apellido(xape);
	atleta1.Nombre(xnom);
	atleta1.Edad(xedad);


	cout<<"\nCedula..."<<atleta1.Cedula();
	cout<<"\nApellido..."<<atleta1.Apellido();
	cout<<"\nNombre..."<<atleta1.Nombre();
	cout<<"\nEdad...."<<atleta1.Edad()<<endl;

}
