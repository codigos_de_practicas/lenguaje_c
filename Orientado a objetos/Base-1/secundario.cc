/************************************************
*Programa: principal.cc				*
*Objetivo: Aplicacion de la clase base		*
*Autor: Axel Díaz / axelio8@gmail.com		*
*Fecha: 06/10/09				*
************************************************/
//{}
#include "base.h"
//no lleva using namespace std, porque el usuario no escribira...

//Funcion estudiante: sólo dará valores a los auxiliares...
Estudiante::Estudiante(string xNombre,string xApellido,double xEdad,double xCI,double xTLF){
  //luego de declarar los auxiliares, les damos los valores:
      this->nombre=xnombre;
      this->apellido=xapellido;
      this->edad=xedad;
      this->ci=xci;
      this->tlf=xtlf;
  //this-> Nombre, para buscar el nombre en la clase Estudiante, pero xNombre pertenece a una funcion nada mas...
}

Estudiante::Entrada(){

	string xnombre, xapellido;
	double xedad, xci, xtlf;

	cout<<"\nNombre.....:"; cin>>xnombre;
	cout<<"\nApellido...:"; cin>>xapellido;
	cout<<"\nEdad.......:"; cin>>xedad;
	cout<<"\nCedula.....:"; cin>>xci;
	cout<<"\nTelefono...:"; cin>>xtlf;
	
	return 1;

}

Estudiante::Salida(){

	string xnombre, xapellido;
	double xedad, xci, xtlf;

	cout<<"\nNombre.....:"<<this->nombre();
	cout<<"\nApellido...:"<<this->apellido();
	cout<<"\nEdad.......:"<<this->edad();
	cout<<"\nCedula.....:"<<this->ci();
	cout<<"\nTelefono...:"<<this->tlf()<<"\n";;
	
	return 1;

}
