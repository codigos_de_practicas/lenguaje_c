/************************************************
*Programa: base.h				*
*Objetivo: Definicion de clase base		*
*Autor: Axel / axelio8@gmail.com		*
*Fecha: 06/10/09				*
************************************************/
//{}
#ifndef _BASE_H
#define _BASE_H
#include <iostream>
#include <string>
using namespace std;

class Estudiante{
      string nombre;
      string apellido;
      double edad;
      double ci;
      double tlf;
public:

      Estudiante(string,string,double,double,double);
      //---------setters y getters-------------//

	void Nombre(string xnombre){this->nombre=xnombre;}
	string Nombre(){return this->nombre;}

	void Apellido(string xapellido){this->apellido=xapellido;}
	string Apellido(){return this->apellido;}

	void Edad(double xedad){this->edad=xedad;}
	double Edad(){return this->edad;}
	
	void CI(double xci){this->ci=xci;}
	double CI(){return this->ci;}

	void TLF(double xtlf){this->tlf=xtlf;}
	double TLF(){return this->tlf;}

};

#endif
