/****************************************************************
* Programa: ferreteria.h					*
* Objetivo: definir la clase Ferreteria				*
* Fecha: 30/09/2009						*
* Autor: jairo molina /www.jairomolina.net.ve/jairo_unerg@hotmail.com
******************************************************************/
#ifndef _FERRETERIA_H
#define _FERRETERIA_H
#include <string>
#include <iostream>
using namespace std;

class Ferreteria{
	int codigo;
	string descripcion;
	int cantidad;
	double precio_c;
public:
	Ferreteria(int,string,int,double);
	//------------> Setter y getter<-----------------------
	void Codigo(int xcodigo){this->codigo=xcodigo;}
	int Codigo(){return this->codigo;}

	void Descripcion(string xdescripcion){this->descripcion=xdescripcion;}
	string Descripcion(){return this->descripcion;}

	void Cantidad(int xcantidad){this->cantidad=xcantidad;}
	int Cantidad(){return this->cantidad;}

	void Precio_c(double xprecio_c){this->precio_c=xprecio_c;}
	double Precio_c(){return this->precio_c;}

	//calculo
	double Inversion(){return this->Cantidad()*this->Precio_c();}

	double Precio_v(){return(this->Precio_c()*0.3)+this->Precio_c();}

	double Ganancia(){return this->Inversion()*0.3;}

	// entrada de datos
	void Entrada();
	void Salida();









};











#endif
