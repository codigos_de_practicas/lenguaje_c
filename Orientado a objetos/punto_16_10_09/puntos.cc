/****************************************************************
* Programa: puntos.cc					*
* Objetivo: implemnetacion de la clase puntos				*
* Fecha: 14/10/2009						*
* Autor: jairo molina /www.jairomolina.net.ve/jairo_unerg@hotmail.com
******************************************************************/
#include "puntos.h"

//--------------> constructor por defecto <----------------
cPunto::cPunto(){
	this->nombre=' ';
	this->abcisa=0;
	this->ordenada=0;
}

//--------------> constructor por paso parametro<----------
cPunto::cPunto(char n,int x,int y){
	this->nombre=n;
	this->abcisa=x;
	this->ordenada=y;
}
//--------------> sobrecarga de entrada <----------
istream & operator >>(istream &entrada,cPunto &objeto){
	int x,y;
	char n;
	cout<<"\nEntrada de Valores";
	cout<<"\nIdentifique el Punto..:";entrada>>n;
	cout<<"\nAbcisa.....:"; entrada>>x;
	cout<<"\nOrdenada...:"; entrada>>y;
	n=toupper(n);
	objeto.Nombre(n);
	objeto.Abcisa(x);
	objeto.Ordenada(y);
	return entrada;
}
//--------------> sobrecarga de entrada <----------
ostream & operator <<(ostream &salida,cPunto &objeto){
	
	cout<<"\n"<<objeto.Nombre()<<" ("<<objeto.Abcisa()<<","<<objeto.Ordenada()<<")"<<endl;
	
	return salida;
}
//--------------> distancia entre dos puntos <---------	
double cPunto::Distancia(int x1,int y1,int x2,int y2){
	return (sqrt(pow((x1-x2),2)+pow((y1-y2),2)));
}

//--------------> pendiente de la recta <---------	
float cPunto::Pendiente(int x1,int y1,int x2,int y2){
	return ((float)(y2-y1)/(x2-x1));
}

//--------------> distancia entre dos puntos <---------	
int cPunto::Paralelo(int x1,int y1,int x2,int y2,int x3,int y3,int x4,int y4){
	float m1,m2;
	m1=(float)this->Pendiente(x1,y1,x2,y2);
	m2=(float)this->Pendiente(x3,y3,x4,y4);
	if(m1==m2) return 1;
	else return 0;
}



















