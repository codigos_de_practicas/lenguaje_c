/****************************************************************
* Programa: puntos.h					*
* Objetivo: definir la clase puntos				*
* Fecha: 14/10/2009						*
* Autor: jairo molina /www.jairomolina.net.ve/jairo_unerg@hotmail.com
******************************************************************/
#ifndef _PUNTOS_H
#define _PUNTOS_H
#include <math.h>
#include <string>
#include <iostream>
using namespace std;

class cPunto{
	char nombre;
	int abcisa;
	int ordenada;
public:
	// constructor
	cPunto();
	cPunto(char,int,int);

	// setters y getters

	void Nombre(char n){this->nombre=n;}
	char Nombre() {return this->nombre;}

	void Abcisa(int x){this->abcisa=x;}
	int Abcisa(){return this->abcisa;}

	void Ordenada(int y){this->ordenada=y;}
	int Ordenada(){return this->ordenada;}

	//calculos 
	double Distancia(int,int,int,int);
	double Pm(int v1,int v2){return((v1+v2)/2);}
	
	float Pendiente(int,int,int,int);

	int Paralelo(int,int,int,int,int,int,int,int);



	//sobrecarga de entrada y salida

	friend istream & operator >>(istream &,cPunto &);	
	friend ostream & operator <<(ostream &,cPunto &);










};
#endif


















