/*********************************************************
* Programa: atleta.h					*
* Objetivo: definicion de la clase Atleta		*
* Autor: jairo molina / jairo_unerg@hotmail.com		*
* Fecha: 30/09/09					*
**********************************************************/
#ifndef _ATLETA_H
#define _ATLETA_H
#include <string>
#include <iostream>
using namespace std;

class Deportista{
	double cedula;
	string apellido;
	string nombre;
	int edad;
	double pago_m;
public:
	Deportista(double,string,string,int,double);
	//-------> setters y getter<--------------------
	void Cedula(double xcedula){this->cedula=xcedula;}
	double Cedula(){return this->cedula;}

	void Apellido(string xapellido){this->apellido=xapellido;}
	string Apellido(){return this->apellido;}
	
	void Nombre(string xnombre){this->nombre=xnombre;}
	string Nombre(){return this->nombre;}

	void Edad(int xedad){this->edad=xedad;}
	int Edad(){return this->edad;}

	void Pago_m(double xpago){this->pago_m=xpago;}
	double Pago_m(){return this->pago_m;}

	//------------> calculos <-------------
	double Iva(){return this->Pago_m()*0.12;}
	double Total(){return (this->Pago_m()+this->Iva());}

	string Mayoria();

	int Entrada();
	int Salida();

};







#endif
