/* ********************Realizado por: Rodrigo Bravo*************
   ********************CI: 19.724.458***************************
   ********************Sección 01******************************* */

#include <iostream>
using namespace std;
#include "arbol.h"
int main(){
  arbol<int>*RB = new arbol<int>;
  int dato, opc, bus;
  char r; 
do{
  
   cout << "\t\t\x1b[91m|*|*|   BIENVENIDO    |*|*|\x1b[m"<< endl;
   cout << "\t\t\x1b[90m|*|Implementación de un Arbol Binario de Busqueda|*|\x1b[m"<< endl;
   cout << "\t\t\x1b[94m1.........INSERTAR\x1b[m"<< endl;
   cout << "\t\t\x1b[94m2.........Ver Recorrido PreOrden\x1b[m"<< endl;
   cout << "\t\t\x1b[94m3.........Ver Recorrido InOrden\x1b[m"<< endl;
   cout << "\t\t\x1b[94m4.........Ver Recorrido PostOrden\x1b[m"<< endl;
   cout << "\t\t\x1b[9m5..........SALIR DEL PROGRAMA\x1b[m"<< endl;
       cin >> opc;
  
switch(opc){
  case 1:
	do{
	cout<<"\n\t\t\x1b[94mIntroduzca un dato en el arbol:\x1b[m"<<endl;
		cin >> dato;
		RB->Insertar_d(dato);
		
		cout << "\t\t\x1b[91m¿Desea ingresar otro dato S/n?\x1b[m" << endl;
		cin >> r; 
	}while(r=='s' || r=='S');
	break;
  case 2:
cout<<"\n\t\tRecorrido en \x1b[92m**PreOrden**\x1b[m"<<flush<<endl;
cout << "\n\t\t";RB->Preorden(RB->Raiz());
cout << "\n" << endl;
  break;
  case 3:
cout<<"\n\t\tRecorrido en \x1b[96m**InOrden**\x1b[m"<<flush<<endl;
cout << "\n\t\t";RB->Inorden(RB->Raiz());
cout << "\n" << endl;
break;
  case 4:
cout<<"\n\t\tRecorrido en \x1b[95m**PostOrden**\x1b[m"<<flush<<endl;
cout << "\n\t\t";RB->Posorden(RB->Raiz());
cout << "\n" << endl;
  break;
  case 5:
    return 1;
    break;
  default:
    cout << "\n\t\t|¡|¡|Opción Invalida|!|!|" << endl;
    break;
}
}while(opc != 6);

return 0;
}
