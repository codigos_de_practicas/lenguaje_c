/*************************************
*Autor:Dervins Brito		     *
*Email: dervins2505@hotmail.com	     *
*Fecha:22-06-2010		     *
*************************************/

/******************************
EL ARBOL MANEJA DATOS ENTEROS
******************************/

#include <iostream>
using namespace std;

class DeRABB {
  private:
  
   class Nodo { // CLASE LOCAL
     public:
      // Constructor:
      Nodo( int data, Nodo *izq=NULL, Nodo *DeR=NULL) :
        dato(data), izquierdo(izq), derecho(DeR) {}
      // Miembros de la Clase
      int dato;
      Nodo *izquierdo;
      Nodo *derecho;
   };
   
/*************************************************
 Punteros  para cabeza y nodo actual
**************************************************/
   Nodo *raiz;
   Nodo *actual;
   int contador;
   int altura;

  public:
   	// Constructores Basicos
//-----------------------------------------------------
   DeRABB() : raiz(NULL), actual(NULL) {}
   ~DeRABB() { Podar(raiz); }


/*****************************
PROTOTIPOS DE LAS FUNCIONES
*****************************/
   void Push( int dat);
   bool Buscar(int dat);
   bool Vacio(Nodo *r) { return r==NULL; }  // Comprueba si el �rbol est� vac�o:
   bool EsHoja(Nodo *r) { return !r->derecho && !r->izquierdo; }  // Funcion que Comprueba si es un nodo hoja:
   int &ValorActual() { return actual->dato; } // Devolver referencia al int del nodo actual:
   void Raiz() { actual = raiz; }  // Moverse al nodo raiz:


/***************************************
Procedimientos para RECORRIDOS DEL ARBOL
***************************************/
   void RecorrerInOrden(void (*func)(int&) , Nodo *nodo=NULL, bool r=true);
   void RecorrerPreOrden(void (*func)(int&) , Nodo *nodo=NULL, bool r=true);
   void RecorrerPostOrden(void (*func)(int&) , Nodo *nodo=NULL, bool r=true);

//----------------------------------------------------------
  private:
   // Funciones auxiliares
   void Podar(Nodo* &);
   void auxContador(Nodo*);
   void auxAltura(Nodo*, int);
};

// Poda: borrar todos los nodos a partir de un nodo, incluido
void DeRABB::Podar(Nodo* &nodo)
{
   if(nodo) {
      Podar(nodo->izquierdo); // Podar izquierdo
      Podar(nodo->derecho);   // Podar derecho
      delete nodo;            // Eliminar nodo
      nodo = NULL;
   }
}


//--------------------------------------------------


/*******************************
Insertar  en el  ABB
********************************/
void DeRABB::Push( int dat)
{
   Nodo *padre = NULL;

   actual = raiz;
   while(!Vacio(actual) && dat != actual->dato) {
      padre = actual;// Busca el dato en el arbol, manteniendo un puntero al nodo padre
      if(dat > actual->dato) {actual = actual->derecho;}
      else if(dat < actual->dato) actual = actual->izquierdo;
   }

   if(!Vacio(actual)) return;  // Si se ha encontrado el elemento, regresar sin insertar
   if(Vacio(padre)) raiz = new Nodo(dat); // Si padre es NULL, entonces el �rbol estaba vac�o, el nuevo nodo sera el nodo raiz
   else if(dat < padre->dato) padre->izquierdo = new Nodo(dat); // Si el entero es menor que el que contiene el nodo padre, lo insertamos en la rama izquierda
   else if(dat > padre->dato) padre->derecho = new Nodo(dat); // Si el intero es mayor que el que contiene el nodo padre, lo insertamos en la rama derecha
}

/**********************
*RECORRIDO INORDEN
**********************/
void DeRABB::RecorrerInOrden(void (*func)(int&) , Nodo *nodo, bool r)
{
   if(r) nodo = raiz;
   if(nodo->izquierdo) RecorrerInOrden(func, nodo->izquierdo, false);
   func(nodo->dato);
   if(nodo->derecho) RecorrerInOrden(func, nodo->derecho, false);
}



/**********************
*RECORRIDO PREORDEN
*********************/
void DeRABB::RecorrerPreOrden(void (*func)(int&), Nodo *nodo, bool r)
{
   if(r) nodo = raiz;
   func(nodo->dato);
   if(nodo->izquierdo) RecorrerPreOrden(func, nodo->izquierdo, false);
   if(nodo->derecho) RecorrerPreOrden(func, nodo->derecho, false);
}


/*******************
*RECORRIDO POSTORDEN
*********************/

void DeRABB::RecorrerPostOrden(void (*func)(int&), Nodo *nodo, bool r)
{
   if(r) nodo = raiz;
   if(nodo->izquierdo) RecorrerPostOrden(func, nodo->izquierdo, false);
   if(nodo->derecho) RecorrerPostOrden(func, nodo->derecho, false);
   func(nodo->dato);
}



//FUNCION PARA MOSTRAR RECORRIDO DEL ARBOL
void Muestra(int &d)
{
   cout << d << ",";
}
